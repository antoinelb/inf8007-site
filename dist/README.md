# Program usage

```
usage: td2.py [-h] [-n N] [-o O] [-d D] [-vt {bow,tfidf}] [-mw MIN_WORD]
              [-mp MAX_PROP] [-mv MAX_VOCAB] [-k K]
              class_

positional arguments:
  class_                Class for which to recommend

optional arguments:
  -h, --help            show this help message and exit
  -n N                  Number of recommendations to make
  -o O                  Output file
  -d D                  Directory containing "PolyHEC"
  -vt {bow,tfidf}, --vec-type {bow,tfidf}
                        Type of vectoriser to use
  -mw MIN_WORD, --min-word MIN_WORD
                        Minimum number of documents where word has to appear
  -mp MAX_PROP, --max-prop MAX_PROP
                        Maximum proportion of documents where word can appear
  -mv MAX_VOCAB, --max-vocab MAX_VOCAB
                        Maximum size of vocabulary to keep
  -k K                  Size of the reduced documents
```

# Test usage

pytest test_td2.py
