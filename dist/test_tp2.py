# pytest and pytest-benchmark are needed to run these tests
import os
from operator import itemgetter

import numpy as np
from scipy.sparse import csc_matrix, csr_matrix

from tp2 import (
    apply_frequency_rules,
    compute_document_frequency,
    compute_predictions,
    compute_similarity,
    compute_term_frequency,
    compute_tfidf,
    read_data,
    reduce_matrix,
    to_indices,
    tokenise,
)


def test_read_data() -> None:
    data, _ = read_data(
        data_dir="../data",
        url="http://www.groupes.polymtl.ca/inf8007/cours.zip",
    )

    assert "inf8007" in map(itemgetter("id"), data)
    assert "stt2700" in map(itemgetter("id"), data)
    assert "mat1720" in map(itemgetter("id"), data)
    assert "act2241" in map(itemgetter("id"), data)

    assert (
        next(x["title"] for x in data if x["id"] == "inf8007")
        == "Langages de script"
    )
    assert (
        next(x["title"] for x in data if x["id"] == "stt2700")
        == "Concepts et methodes en statistique"
    )
    assert (
        next(x["title"] for x in data if x["id"] == "mat1720")
        == "Probabilites"
    )
    assert (
        next(x["title"] for x in data if x["id"] == "act2241")
        == "Produits derives et gestion de risque"
    )

    assert next(x["description"] for x in data if x["id"] == "inf8007") == (
        "Caracteristiques des langages de script. Principaux langages et "
        "domaines d'application. Programmation avec un langage de script : "
        "syntaxe, structures de controle, structures de donnees, "
        "communication interprocessus et communication avec une base de "
        "donnees, modules clients et serveurs."
    )
    assert next(x["description"] for x in data if x["id"] == "stt2700") == (
        "Estimation ponctuelle et par intervalle. Tests d'hypotheses. "
        "Methodes graphiques. Test du khi-deux. Theorie de la decision et "
        "inference bayesienne. Comparaisons de deux echantillons."
    )
    assert next(x["description"] for x in data if x["id"] == "mat1720") == (
        "Espace de probabilite. Analyse combinatoire. Probabilite "
        "conditionnelle. Independance. Variable aleatoire. Fonction de "
        "repartition et fonction generatrice. Esperance mathematique. Loi "
        "faible des grands nombres. Theoreme limite central."
    )
    assert next(x["description"] for x in data if x["id"] == "act2241") == (
        "Contrats a terme, a livrer, d'echange, gestion de risque, options, "
        "arbres binomiaux, formule de Black-Scholes, couverture en delta, "
        "options exotiques, lemme d'Ito, modeles des taux d'interet. Ce cours "
        "contribue a la preparation des SOA MF et MFE."
    )


def test_tokenise() -> None:
    test = []
    test.append(("Le chat noir.",))
    test.append(("L'esprit et le corps",))
    test.append(("L'apres-midi",))

    correct = []
    correct.append(["le", "chat", "noir", "."])
    correct.append(["l'", "esprit", "et", "le", "corps"])
    correct.append(["l'", "apres-midi"])

    function = [tokenise(*t) for t in test]

    for i in range(len(test)):
        assert function[i] == correct[i], f"Failed for test {i + 1}"


def test_to_indices() -> None:

    sentences = [
        "Il y a un chat!",
        "Un noir?",
        "Je suis pas mal sur qu'il est vert.",
        "C'est la faute aux extra-terrestres.",
    ]
    text = [tokenise(sentence) for sentence in sentences]

    indices, w2i = to_indices(text)

    correct = [
        [w2i["il"], w2i["y"], w2i["a"], w2i["un"], w2i["chat"], w2i["!"]],
        [w2i["un"], w2i["noir"], w2i["?"]],
        [
            w2i["je"],
            w2i["suis"],
            w2i["pas"],
            w2i["mal"],
            w2i["sur"],
            w2i["qu'"],
            w2i["il"],
            w2i["est"],
            w2i["vert"],
            w2i["."],
        ],
        [
            w2i["c'"],
            w2i["est"],
            w2i["la"],
            w2i["faute"],
            w2i["aux"],
            w2i["extra-terrestres"],
            w2i["."],
        ],
    ]

    assert indices == correct


def test_compute_term_frequency(benchmark) -> None:
    data = ([[0, 1, 1, 2, 2, 2], [2, 2, 2, 3, 3, 4]], 5)

    correct = csc_matrix([[1, 2, 3, 0, 0], [0, 0, 3, 2, 1]])

    result = benchmark(compute_term_frequency, *data)

    assert isinstance(result, csc_matrix)
    assert not (result != correct).sum()


def test_compute_document_frequency(benchmark) -> None:
    data = (csc_matrix([[1, 2, 3, 0, 0], [0, 0, 3, 2, 1]]),)

    correct = np.array([1, 1, 2, 1, 1])

    result = benchmark(compute_document_frequency, *data)

    assert isinstance(result, np.ndarray)
    assert not (result != correct).sum()


def test_compute_tfidf(benchmark) -> None:
    correct = csr_matrix(
        [[0.693_147, 1.38629, 0, 0, 0], [0, 0, 0, 1.38629, 0.693_147]]
    )
    data = (
        csc_matrix([[1, 2, 3, 0, 0], [0, 0, 3, 2, 1]]),
        np.array([1, 1, 2, 1, 1]),
    )

    result = benchmark(compute_tfidf, *data)

    assert isinstance(result, csr_matrix)
    assert abs((result - correct).sum()) < 1e-4


def test_apply_frequency_rules() -> None:
    test = []
    test.append(
        (
            csc_matrix([[1, 2, 3, 0, 0], [0, 4, 3, 2, 1]]),
            np.array([1, 2, 2, 1, 1]),
            2,
            1.0,
            5,
        )
    )
    test.append(
        (
            csc_matrix([[1, 2, 3, 0, 0], [0, 4, 3, 2, 1]]),
            np.array([1, 2, 2, 1, 1]),
            1,
            0.5,
            5,
        )
    )
    test.append(
        (
            csc_matrix([[1, 2, 3, 0, 0], [0, 4, 3, 2, 1]]),
            np.array([1, 2, 2, 1, 1]),
            1,
            1,
            2,
        )
    )

    correct = []
    correct.append((csc_matrix([[2, 3], [4, 3]]), np.array([2, 2])))
    correct.append((csc_matrix([[1, 0, 0], [0, 2, 1]]), np.array([1, 1, 1])))
    correct.append((csc_matrix([[2, 3], [4, 3]]), np.array([2, 2])))

    function = [apply_frequency_rules(*t) for t in test]

    for i in range(len(test)):
        assert not (
            function[i][0] != correct[i][0]
        ).sum(), "Failed for test {i + 1}"
        assert not (
            function[i][1] != correct[i][1]
        ).sum(), "Failed for test {i + 1}"


def test_reduce_matrix() -> None:
    if not os.path.exists("cache"):
        os.mkdir("cache")
    test = []
    test.append((csr_matrix(np.random.randn(20, 30)), "fake-hash", ".", 15, 1))

    correct = []
    correct.append((20, 15))

    function = [reduce_matrix(*t)[0].shape for t in test]

    for i in range(len(test)):
        assert function[i] == correct[i], "Failed for test {i + 1}"

    os.remove(
        os.path.join(
            os.path.dirname(__file__), "cache", "reduced_15_1_fake-hash.pkl"
        )
    )
    if not os.listdir("cache"):
        os.rmdir("cache")


def test_compute_similarity() -> None:
    test = []
    test.append((np.array([[1, 2, 3], [4, 5, 6]]), np.array([3, 2, 1])))

    correct = []
    correct.append(np.array([0.714_286, 0.852_803]))

    function = [compute_similarity(*t) for t in test]

    for i in range(len(test)):
        assert (
            abs((function[i] - correct[i]).sum()) < 1e-4
        ), f"Failed for test {i + 1}"


def test_compute_predictions() -> None:
    test = []
    test.append((np.array([0.5, 0.25, 4, 2, 1.25]), 3))

    correct = []
    correct.append((np.array([2, 3, 4]), np.array([4, 2, 1.25])))

    function = [compute_predictions(*t) for t in test]

    for i in range(len(test)):
        for j in range(len(function[i])):
            assert not (
                function[i][j] != correct[i][j]
            ).sum(), f"Failed for test {i + 1}"
