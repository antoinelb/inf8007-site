from argparse import ArgumentParser, FileType
import io
import hashlib
import itertools
import os
from operator import itemgetter
import pickle
import re
import unicodedata
from functools import reduce
from typing import Any, Dict, List, Set, Tuple, Union

import numpy as np  # type: ignore
from nltk.stem.snowball import FrenchStemmer  # type: ignore
from scipy.sparse import csc_matrix, csr_matrix  # type: ignore
from scipy.sparse.linalg import svds  # type: ignore

#############
### Types ###
#############
Data = List[Dict[str, str]]
PreprocessedText = List[List[int]]
Word2Index = Dict[str, int]
IntermediateText = Tuple[List[str], List[str], List[List[str]], List[List[str]]
                        ]
Recommendation = Dict[str, Union[str, float]]

##########################
### External functions ###
##########################


def read_data(base_dir: str) -> Tuple[Data, str]:
    """Reads the data and returns data and hash.

    Parameters
    ----------
    base_dir : str
        Directory containing "PolyHEC" and where to put cache

    Returns
    -------
    data : Data
        [{
            id: str
            title: str
            description: str
        }]
    hash_ : str
        Hash of the data
    """
    assert os.path.exists(base_dir) and os.path.isdir(base_dir)

    print("[*] Reading:".ljust(80), end="\r")

    # paths for the text and cached data
    path = os.path.join(base_dir, "PolyHEC")
    pkl_path = os.path.join(base_dir, "cache")
    if not os.path.exists(pkl_path):
        os.mkdir(pkl_path)
    pkl_path = os.path.join(pkl_path, "cours.pkl")

    if os.path.exists(pkl_path):

        # read cached data and hash for the data
        print("[*] Reading: reading cache...".ljust(80), end="\r")
        with open(pkl_path, "rb") as pkl_f:
            data, hash_ = pickle.load(pkl_f)

    else:

        if not os.path.exists(path):
            raise IOError(
                "The class directory doesn't exist. Make sure the data was unzipped."
            )

        # reads the data file by file and adds it to a list as dict containing
        # the "id" (class code), the "title" and the "description"
        n = len(os.listdir(path))
        data = []
        hash_ = None
        for i, class_ in enumerate(os.listdir(path)):
            if os.path.isfile(os.path.join(path, class_)):
                with open(os.path.join(path, class_), "r") as f:
                    c = re.sub(r"[\s\n]+", " ", f.read().strip())
                    title, description = re.match(
                        r"^TitreCours:\s?(.*)\s+DescriptionCours:\s?(.*)$", c
                    ).groups()
                    data.append({
                        "id": class_[:-4].lower(),
                        "title": title,
                        "description": description
                    })
                print(
                    "[*] Reading: reading class {{0:>{0:}}}/{{1:}}: {{2:}}...".
                    format(len(str(n))).format(i + 1, n, class_).ljust(80),
                    end="\r"
                )

        # computes the data hash and saves the data as pickled file
        print("[*] Reading: hashing...".ljust(80), end="\r")
        hash_ = hash_data(data)
        print("[*] Reading: caching...".ljust(80), end="\r")
        with open(pkl_path, "wb") as pkl_f:
            pickle.dump((data, hash_), pkl_f, pickle.HIGHEST_PROTOCOL)
    print("[+] Reading: done.".ljust(80))
    return data, hash_


def recommend(
    class_id: str,
    data: Data,
    data_hash: str,
    base_dir: str,
    n: int,
    include_class: bool = False,
    vec_type: str = "tfidf",
    min_word_count: int = 1,
    max_word_prop: float = 1,
    max_vocab_size: int = 50000,
    k: int = 32
) -> List[Recommendation]:
    """Recommends `n` classes for `class_id` from `data` plus the class if
    asked for.

    Parameters
    ----------
    class_id : str
        Class to compare to
    data : Data
        Data as returned by read_data
    data_hash : str
        Hash of the data
    base_dir : str
        Directory containing "PolyHEC" and where to put cache
    n : int
        Number of recommendations made
    include_class : bool (default : False)
        If class_id should be added to returned values
    vec_type: "bow" or "tfidf" (default : "tfidf")
        Type of vectoriser to use
    min_word_count : int (default : 1)
        Minimum number of expertise where a word appears for it to be kept
    max_word_prop : float (default : 1)
        Maximum proportion where a word can be for it to be kept
    max_vocab_size : int (default : 50000)
        Maximum vocabulary size
    k : int (default : 32)
        Size of the reduced matrix

    Returns
    -------
    recommendations : List[Recommendation]
        Recommendations and their similarity
    """
    assert os.path.exists(base_dir) and os.path.isdir(base_dir)
    assert n > 0

    class2idx = {c: i for i, c in enumerate(map(itemgetter("id"), data))}
    assert class_id in class2idx

    class_idx = class2idx[class_id]

    preprocessed, _, hash_ = preprocess(data, data_hash, base_dir)
    matrix, hash_ = vectorise(
        preprocessed, hash_, base_dir, vec_type, min_word_count, max_word_prop,
        max_vocab_size
    )
    reduced, hash_ = reduce_matrix(matrix, hash_, base_dir, k)
    recommendations_ = compute_recommendations(reduced, class_idx, n)

    idx2class = {i: c for c, i in class2idx.items()}
    recommendations = [
        {
            "recommendation": idx2class[r["recommendation"]],  # type: ignore
            "similarity": r["similarity"]
        } for r in recommendations_
    ]

    return recommendations


def write_result(
    class_id: str, data: Data, recommendations: List[Recommendation],
    file_: io.TextIOWrapper
) -> None:
    """Writes the result to `file_`.

    Parameters
    ----------
    class_id : str
        Class to compare to
    data : Data
        Class data
    recommendations : List[Recommendation]
        Recommendations and their similarity
    file_ : TextIOWrapper
        File where to write the results
    """
    class2index = {c: i for i, c in enumerate(map(itemgetter("id"), data))}
    file_.write(f"Compared class : {class_id}\n")
    file_.write("Titre: {}\n".format(data[class2index[class_id]]["title"]))
    file_.write(
        "Description: {}\n".format(data[class2index[class_id]]["description"])
    )
    file_.write("\n")
    for r in recommendations:
        file_.write(
            "{0:} : {1:.5f}\n".format(r["recommendation"], r["similarity"])
        )
        file_.write(
            "Titre : {}\n".format(
                data[class2index[r["recommendation"]]]["title"]
            )
        )
        file_.write(
            "Description : {}\n".format(
                data[class2index[r["recommendation"]]]["description"]
            )
        )
        file_.write("\n")
    file_.close()


##########################
### Internal functions ###
##########################

#########################
### General functions ###
#########################


def hash_data(data: Any) -> str:
    """Creates a hash from the data.

    Parameters
    ----------
    data : any
        Structure to hash

    Returns
    -------
    hash_ : str
        Hash
    """
    hash_ = hashlib.md5(pickle.dumps(data)).hexdigest()
    return hash_


###############################
### Preprocessing functions ###
###############################


def preprocess(data: Data, hash_: str, base_dir: str
              ) -> Tuple[PreprocessedText, IntermediateText, str]:
    """Preprocesses the text data.

    Parameters
    ----------
    data : Data
        Data as returned by read_data
    hash_ : str
        Hash of the inputs
    base_dir : str
        Directory containing "PolyHEC" and where to put cache

    Returns
    -------
    preprocessed : PreprocessedText
        Preprocessed documents
    intermediate_text : IntermediateText
        Intermediate representations of the data (combined, ascii, tokenised, stemmed)
    hash_ : str
        Hash of the outputs
    """
    print("[*] Preprocessing:".ljust(80), end="\r")

    pkl_path = os.path.join(base_dir, "cache", f"{hash_}_preprocessed.pkl")

    if os.path.exists(pkl_path):
        print("[*] Preprocessing: reading cache...".ljust(80), end="\r")
        with open(pkl_path, "rb") as f:
            preprocessed, intermediate_text, hash_ = pickle.load(f)

    else:
        print(
            "[*] Preprocessing: combining title and description...".ljust(80),
            end="\r"
        )
        combined = [d["title"] + " " + d["description"] for d in data]
        print("[*] Preprocessing: converting to ascii...".ljust(80), end="\r")
        ascii_ = list(map(to_ascii, combined))
        print("[*] Preprocessing: tokenising...".ljust(80), end="\r")
        tokenised = list(map(tokenise, ascii_))
        print("[*] Preprocessing: stemming...".ljust(80), end="\r")
        stemmed = stem(tokenised)
        intermediate_text = (combined, ascii_, tokenised, stemmed)

        print(
            "[*] Preprocessing: converting to indices...".ljust(80), end="\r"
        )
        preprocessed, _ = to_indices(stemmed)

        print("[*] Preprocessing: hashing...".ljust(80), end="\r")
        hash_ = hash_data((preprocessed, intermediate_text))

        print("[*] Preprocessing: caching...".ljust(80), end="\r")
        with open(pkl_path, "wb") as f:
            pickle.dump((preprocessed, intermediate_text, hash_), f,
                        pickle.HIGHEST_PROTOCOL)

    print("[+] Preprocessing: done.".ljust(80))

    return preprocessed, intermediate_text, hash_


def to_ascii(text: str) -> str:
    """Removes or converts non-ascii characters.

    Parameters
    ----------
    text : str
        Text to convert

    Returns
    -------
    ascii_ : str
        Ascii text
    """
    ascii_ = "".join(
        c for c in unicodedata.normalize("NFD", text.strip())
        if unicodedata.category(c) != "Mn" and ord(c) < 128
    )
    return ascii_


def tokenise(text: str) -> List[str]:
    """Tokenises the given text using french rules.

    Parameters
    ----------
    text : str
        Text to tokenise

    Returns
    -------
    tokenised : list of str
        Tokenised text
    """
    rules = [
        (r"(?:^\s+)|(?:\s+$)", ""),
        (r"(?<=[a-z])([^a-zA-Z\d_\-'])", r" \1"),
        (r"'", r"' "),
        (r"\"\b", "\" "),
    ]

    def apply_rule(current, rule):
        return re.sub(rule[0], rule[1], current)

    text = text.lower()
    text = reduce(apply_rule, rules, text)
    tokenised = re.split(r"\s+", text)
    return tokenised


def stem(text: List[List[str]]) -> List[List[str]]:
    """Stems the given text using the nltk French snowball stemmer.

    Parameters
    ----------
    text : list-2 of str
        Words to stem

    Returns
    -------
    stemmed : list-2 of str
        Stemmed words
    """
    stemmer = FrenchStemmer()
    stemmed = [[stemmer.stem(word) for word in sentence] for sentence in text]
    return stemmed


def to_indices(text: List[List[str]]
              ) -> Tuple[List[List[int]], Dict[str, int]]:
    """Converts the given text to indices and returns a dict to convert words
    to indices.

    Parameters
    ----------
    text : list-2 of str
        Text to convert

    Returns
    -------
    indices : list-2 of int
        Converted text
    word2index : dict of str to int
        Dict to convert words
    """
    vocab: Set[str] = set().union(*map(set, text))  # type: ignore
    word2index = {w: i for i, w in enumerate(vocab)}
    indices = [[word2index[word] for word in sentence] for sentence in text]
    return indices, word2index


############################
### Similarity functions ###
############################


def vectorise(
    preprocessed: PreprocessedText, hash_: str, base_dir: str, vec_type: str,
    min_word_count: int, max_word_prop: float, max_vocab_size: int
) -> Tuple[csr_matrix, str]:
    """Vectorises the given data.

    Parameters
    ----------
    preprocessed : PreprocessedText
        Preprocessed documents
    hash_ : str
        Hash of the inputs
    base_dir : str
        Directory containing "PolyHEC" and where to put cache
    vec_type : "bow" or "tfidf"
        Vectoriser type
    min_word_count : int
        Minimum occurence of word in documents
    max_word_prop : float
        Maximum frequency of word in documents
    max_vocab_size : int
        Maximum size of the vocabulary

    Returns
    -------
    matrix : csr_matrix of size n_documents x n_vocab
        Vectors of the documents
    hash_ : str
        Hash of the outputs
    """
    assert vec_type == "bow" or vec_type == "tfidf"
    assert min_word_count > 0
    assert 0 < max_word_prop <= 1
    assert max_vocab_size > 0

    print("[*] Vectorising:".ljust(80), end="\r")

    pkl_path = os.path.join(
        base_dir, "cache",
        f"{hash_}_{vec_type}_{min_word_count}_{max_word_prop}_{max_vocab_size}.pkl"
    )

    if os.path.exists(pkl_path):
        print("[*] Vectorising: reading cache...".ljust(80), end="\r")
        with open(pkl_path, "rb") as f:
            matrix, hash_ = pickle.load(f)

    else:
        print("[*] Vectorising: extracting vocabulary...".ljust(80), end="\r")
        n_vocab = max([xx for x in preprocessed for xx in x]) + 1
        print(
            "[*] Vectorising: computing term frequency...".ljust(80), end="\r"
        )
        tf = compute_term_frequency(preprocessed, n_vocab)
        print(
            "[*] Vectorising: computing document frequency...".ljust(80),
            end="\r"
        )
        df = compute_document_frequency(tf)
        print(
            "[*] Vectorising: applying frequency rules...".ljust(80), end="\r"
        )
        tf, df = apply_frequency_rules(
            tf, df, min_word_count, max_word_prop, max_vocab_size
        )
        if vec_type == "bow":
            matrix = csr_matrix(tf)
        else:
            print("[*] Vectorising: computing TF-IDF...".ljust(80), end="\r")
            matrix = compute_tfidf(tf, df)
        print("[*] Vectorising: hashing...".ljust(80), end="\r")
        hash_ = hash_data(matrix)
        print("[*] Vectorising: caching...".ljust(80), end="\r")
        with open(pkl_path, "wb") as f:
            pickle.dump((matrix, hash_), f, pickle.HIGHEST_PROTOCOL)

    print("[+] Vectorising: done.".ljust(80))

    return matrix, hash_


def compute_term_frequency(indices: List[List[int]], n_out: int) -> csc_matrix:
    """Counts the occurence of each index in indices.

    Parameters
    ----------
    indices : list-2 of int
        Indices for all documents
    n_out : int
        Number of indices

    Returns
    -------
    tf : csc_matrix of size n_documents x n_out
        Term frequency
    """
    counts = [{index: document.count(index)
               for index in set(document)}
              for document in indices]
    data = [count for document in counts for count in document.values()]
    cols = [col for document in counts for col in document.keys()]
    rows = [
        i for i, document in enumerate(counts) for _ in enumerate(document)
    ]
    tf = csc_matrix((data, (rows, cols)), (len(indices), n_out))
    return tf


def compute_document_frequency(tf: csc_matrix) -> np.ndarray:
    """Counts the occurence of the presence of terms in documents.

    Parameters
    ----------
    tf : csc_matrix of size n_documents x n_out
        Term frequency

    Returns
    -------
    df : array-1 of size n_out
        Document frequency
    """
    df = tf.getnnz(0)
    return df


def compute_tfidf(tf: csc_matrix, df: np.ndarray) -> csr_matrix:
    """Computes the TF-IDF matrix.

    Parameters
    ----------
    tf : csc_matrix of size n_documents x n_out
        Term frequency
    df : array-1 of size n_out
        Document frequency

    Returns
    -------
    tfidf : csr_matrix of size n_documents x n_out
        TF-IDF
    """
    idf = np.log(tf.shape[0] / df)
    tfidf = csr_matrix(tf)
    tfidf.data = tfidf.data * np.take(idf, tfidf.indices)
    tfidf.eliminate_zeros()
    return tfidf


def apply_frequency_rules(
    tf: csc_matrix, df: np.ndarray, min_word_count: int, max_word_prop: float,
    max_vocab_size: int
) -> Tuple[csr_matrix, np.ndarray]:
    """Adjust the term frequency and document frequency with the given rules.

    Parameters
    ----------
    tf : csc_matrix of size n_documents x n_out
        Term frequency
    df : array-1 of size n_out
        Document frequency
    min_word_count : int
        Minimum number of expertise where a word appears for it to be kept
    max_word_prop : float
        Maximum proportion where a word can be for it to be kept
    max_vocab_size : int
        Maximum vocabulary size

    Returns
    -------
    tf : csc_matrix of size n_documents x n_out
        Adjusted term frequency
    df : array-1 of size n_out
        Adjusted document frequency
    """
    n_documents = tf.shape[0]
    kept_indices = list(
        itertools.islice(
            map(
                itemgetter(0),
                sorted(((i, count)
                        for i, count in enumerate(df)
                        if count >= min_word_count and
                        count / n_documents <= max_word_prop),
                       key=itemgetter(1),
                       reverse=True)
            ), max_vocab_size
        )
    )

    tf = tf[:, kept_indices]
    df = df[kept_indices]

    return tf, df


###########################
### Reduction functions ###
###########################


def reduce_matrix(matrix: csr_matrix, hash_: str, base_dir: str,
                  k: int) -> Tuple[np.ndarray, str]:
    """Reduces the given matrix columns using SVD.

    Parameters
    ----------
    matrix : csr_matrix of size n_documents x n_vocab
        Matrix to reduce
    hash_ : str
        Hash of the inputs
    base_dir : str
        Directory containing "PolyHEC" and where to put cache
    k : int
        Size of the reduced matrix

    Returns
    -------
    reduced : array-2 of size n_documents x k
        Reduced matrix
    hash_ : str
        Hash of the outputs
    """
    assert 0 < k < min(matrix.shape)
    print("[*] Reducing:".ljust(80), end="\r")

    pkl_path = os.path.join(base_dir, "cache", f"{hash_}_reduced_{k}.pkl")

    if os.path.exists(pkl_path):
        print("[*] Reducing: reading cache...".ljust(80), end="\r")
        with open(pkl_path, "rb") as f:
            reduced, hash_ = pickle.load(f)

    else:
        print("[*] Reducing: computing SVD...".ljust(80), end="\r")
        U, D, _ = svds(matrix, k)
        reduced = U * D
        print("[*] Reducing: hashing...".ljust(80), end="\r")
        hash_ = hash_data(reduced)
        print("[*] Reducing: caching...".ljust(80), end="\r")
        with open(pkl_path, "wb") as f:
            pickle.dump((reduced, hash_), f, pickle.HIGHEST_PROTOCOL)

    print("[+] Reducing: done.".ljust(80))

    return reduced, hash_


#################################
### Recommendations functions ###
#################################


def compute_recommendations(reduced: np.ndarray, idx: int,
                            n: int) -> List[Dict[str, Union[int, float]]]:
    """Computes the recommendations for the given row `idx`.

    Parameters
    ----------
    reduced : array-2 of size n_documents x k
        Reduced matrix
    idx : int
        Index of the row compared to
    n : int
        Number of predictions to make

    Returns
    -------
    recommendations : List[Recommendation]
        Each element contains the "recommendation" and "similarity"
    """
    print("[*] Recommending:".ljust(80), end="\r")
    print("[*] Recommending: computing similarity...".ljust(80), end="\r")
    similiarities = compute_similarity(reduced, reduced[idx])
    print("[*] Recommending: computing predictions...".ljust(80), end="\r")
    predictions, similiarities = compute_predictions(similiarities, n)
    recommendations = [{
        "recommendation": r,
        "similarity": s
    } for r, s in zip(predictions, similiarities)]
    print("[+] Recommending: done.".ljust(80))
    return recommendations


def compute_similarity(matrix: np.ndarray, vector: np.ndarray) -> np.ndarray:
    """Computes the cosine similarity between rows of `matrix` and `vector`.

    Parameters
    ----------
    matrix : array-2 of size n_documents x k
        Matrix containing all the data
    vector : array-1 of size k
        Vector to compare to

    Returns
    -------
    similiarities : array-1 of size n_documents
        Similarities
    """
    matrix = matrix / (np.linalg.norm(matrix, axis=1, keepdims=True) + 1e-16)
    vector = vector / (np.linalg.norm(vector) + 1e-16)
    similiarities = np.dot(matrix, vector)
    return similiarities


def compute_predictions(similiarities: np.ndarray,
                        n: int) -> Tuple[np.ndarray, np.ndarray]:
    """Compute the `n` best predictions given `similiarities`.

    Parameters
    ----------
    similiarities : array-1 of size n_documents
        Similarities
    n : int
        Number of predictions to make

    Returns
    -------
    predictions : array-1 of size n
        Predicted indices
    similiarities_ : array-1 of size n
        Corresponding similiarities
    """
    predictions = np.argpartition(-similiarities, n)[:n]
    similiarities_ = similiarities[predictions]

    sorting_indices = np.argsort(-similiarities_)
    predictions = predictions[sorting_indices]
    similiarities_ = similiarities_[sorting_indices]

    return predictions, similiarities_


############
### Main ###
############
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "class_", type=str, help="Class for which to recommend"
    )
    parser.add_argument(
        "-n", type=int, default=5, help="Number of recommendations to make"
    )
    parser.add_argument(
        "-o",
        type=FileType("w"),
        default=open("recommendations.txt", "w"),
        help="Output file"
    )
    parser.add_argument(
        "-d", type=str, default=".", help="Directory containing \"PolyHEC\""
    )
    parser.add_argument(
        "-vt",
        "--vec-type",
        choices=["bow", "tfidf"],
        default="tfidf",
        help="Type of vectoriser to use"
    )
    parser.add_argument(
        "-mw",
        "--min-word",
        type=int,
        default=1,
        help="Minimum number of documents where word has to appear"
    )
    parser.add_argument(
        "-mp",
        "--max-prop",
        type=float,
        default=1,
        help="Maximum proportion of documents where word can appear"
    )
    parser.add_argument(
        "-mv",
        "--max-vocab",
        type=int,
        default=50000,
        help="Maximum size of vocabulary to keep"
    )
    parser.add_argument(
        "-k", type=int, default=32, help="Size of the reduced documents"
    )
    args = parser.parse_args()

    data, hash_ = read_data(args.d)
    recommendations = recommend(
        args.class_, data, hash_, args.d, args.n, False, args.vec_type,
        args.min_word, args.max_prop, args.max_vocab, args.k
    )
    write_result(args.class_, data, recommendations, args.o)
