#!/usr/bin/python3
import argparse
import hashlib
import io
import itertools
import os
import pickle
import re
import time
import zipfile
from functools import reduce
from operator import itemgetter
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np
import requests
from nltk.stem.snowball import FrenchStemmer
from scipy.sparse import csc_matrix, csr_matrix
from scipy.sparse.linalg import svds

#########
# Setup #
#########

Data = List[Dict[str, str]]
Recommendation = Dict[str, Union[str, float]]


def read_args() -> argparse.Namespace:
    """
    Parses the command line arguments and returns them as a namespace.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "class_", type=str, help="Class for which to recommend"
    )
    parser.add_argument(
        "-n",
        "--n-recommendations",
        type=int,
        default=5,
        help="Number of recommendations to make",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        default="recommendations.txt",
        help="Output file",
    )
    parser.add_argument(
        "-vt",
        "--vec-type",
        choices=["bow", "tfidf"],
        default="tfidf",
        help="Type of vectoriser to use",
    )
    parser.add_argument(
        "-mw",
        "--min-word",
        type=int,
        default=1,
        help="Minimum number of documents where word has to appear",
    )
    parser.add_argument(
        "-mp",
        "--max-prop",
        type=float,
        default=1,
        help="Maximum proportion of documents where word can appear",
    )
    parser.add_argument(
        "-mv",
        "--max-vocab",
        type=int,
        default=50000,
        help="Maximum size of vocabulary to keep",
    )
    parser.add_argument(
        "-k",
        "--n-reduced",
        type=int,
        default=64,
        help="Size of the reduced documents",
    )
    parser.add_argument(
        "-d",
        "--data-dir",
        type=str,
        default=".",
        help="Directory containing PolyHEC",
    )
    parser.add_argument(
        "-u",
        "--url",
        type=str,
        default="http://www.groupes.polymtl.ca/inf8007/cours.zip",
        help="Url link to the data",
    )
    return parser.parse_args()


def read_data(data_dir: str, url: str) -> Tuple[Data, str]:
    """
    Reads the data if in the data directory or downloads and saves it from the
    url if not.

    Parameters
    ----------
    data_dir : str
        Directory containing the PolyHEC directory with class files
    url : str
        Url for the data

    Returns
    -------
    Data
        Data under the format:
            [{
                id: str
                title: str
                description: str
            }]
    str
        String representing the data
    """
    start = time.time()
    path = os.path.join(data_dir, "PolyHEC")
    pkl_path = os.path.join(data_dir, "cache")

    if not os.path.exists(pkl_path):
        os.makedirs(pkl_path, exist_ok=True)
    pkl_path = os.path.join(pkl_path, "cours.pkl")

    if os.path.exists(pkl_path):

        print("[*] Reading data: reading cache...".ljust(80), end="\r")
        with open(pkl_path, "rb") as pkl_f:
            data, hash_ = pickle.load(pkl_f)

    else:

        if not os.path.exists(path) or not os.listdir(path):
            print("[*] Reading data: downloading data...".ljust(80), end="\r")
            resp = requests.get(url)
            if resp.ok:
                if resp.headers["content-type"] != "application/zip":
                    print()
                    raise ValueError(f"The url {url} isn't valid.")
                else:
                    with zipfile.ZipFile(io.BytesIO(resp.content)) as zip_f:
                        zip_f.extractall(data_dir)
            else:
                print()
                raise ValueError(f"The url {url} isn't valid.")

        files = os.listdir(path)
        data = []

        for i, class_ in enumerate(os.listdir(path)):
            file_ = os.path.join(path, class_)
            print(
                (
                    "[*] Reading data: reading class {{0:>{0:}}}/".format(
                        len(str(len(files)))
                    ).format(i + 1)
                    + f"{len(files)}: "
                    + f"{class_}..."
                ).ljust(80),
                end="\r",
            )
            if os.path.isfile(file_):
                with open(file_, "r") as f:
                    content = re.sub(r"[\s\n]+", " ", f.read().strip())
                    title, description = re.match(
                        r"^TitreCours:\s*(.*)\s+DescriptionCours:\s*(.*)$",
                        content,
                    ).groups()
                    data.append(
                        {
                            "id": class_[:-4].lower(),
                            "title": title,
                            "description": description,
                        }
                    )

        print("[*] Reading data: hashing...".ljust(80), end="\r")
        hash_ = hash_data(data)

        print("[*] Reading data: caching...".ljust(80), end="\r")
        with open(pkl_path, "wb") as pkl_f:
            pickle.dump((data, hash_), pkl_f, pickle.HIGHEST_PROTOCOL)

    duration = time.time() - start

    print(f"[+] Reading data: done in {duration:<.2f} s.".ljust(80))
    return data, hash_


##################
# Main functions #
##################


def recommend(
    class_id: str,
    data: Data,
    data_hash: str,
    data_dir: str,
    n_recommendations: int = 5,
    include_class: bool = False,
    vec_type: str = "tfidf",
    min_word_count: int = 1,
    max_word_prop: float = 1,
    max_vocab_size: int = 50000,
    n_reduced: int = 64,
) -> List[Recommendation]:
    """
    Recommends `n` classes for `class_id` from `data` plus the class if asked
    for.

    Parameters
    ----------
    class_id : str
        Class to compare to
    data : Data
        Data as returned by read_data
    data_hash : str
        Hash of the data
    data_dir : str
        Directory containing "PolyHEC" and where to put cache
    n_recommendations : int (default : 5)
        Number of recommendations made
    include_class : bool (default : False)
        If class_id should be added to returned values
    vec_type: "bow" or "tfidf" (default : "tfidf")
        Type of vectoriser to use
    min_word_count : int (default : 1)
        Minimum number of expertise where a word appears for it to be kept
    max_word_prop : float (default : 1)
        Maximum proportion where a word can be for it to be kept
    max_vocab_size : int (default : 50000)
        Maximum vocabulary size
    n_reduced : int (default : 64)
        Size of the reduced matrix

    Returns
    -------
    Dict[str, List[Recommendation]]
        Recommendations and their similarity under the format:
            {
                class_id: [{
                    id: str,
                    similarity: float
                }]
            }
    """
    class2index = {
        class_: i for i, class_ in enumerate(map(itemgetter("id"), data))
    }
    index2class = {i: class_ for class_, i in class2index.items()}

    class_idx = class2index[class_id]

    preprocessed, _, _, hash_ = preprocess(data, data_hash, data_dir)

    vectorized, hash_ = vectorize(
        preprocessed,
        hash_,
        data_dir,
        vec_type,
        min_word_count,
        max_word_prop,
        max_vocab_size,
    )

    reduced, hash_ = reduce_matrix(
        vectorized, hash_, data_dir, n_reduced, norm_by=1
    )

    recommendations = compute_recommendations(
        reduced, class_idx, n_recommendations, normed=True
    )

    return {
        class_id: [
            {
                "recommendation": index2class[r["recommendation"]],
                "similarity": r["similarity"],
            }
            for r in recommendations
        ]
    }


def write_result(
    recommendations: Dict[str, List[Recommendation]],
    data: Data,
    output_file: str,
) -> None:
    """
    Writes the result to `output_file`.

    Parameters
    ----------
    recommendations : Dict[str, List[Recommendation]]
        Recommendations and their similarity
    data : Data
        Class data
    output_file : str
        File where to write the results
    """
    class2index = {c: i for i, c in enumerate(map(itemgetter("id"), data))}
    class_id = next(iter(recommendations.keys()))
    with open(output_file, "w") as f:
        f.write(f"Compared class : {class_id}\n")
        f.write("Titre: {}\n".format(data[class2index[class_id]]["title"]))
        f.write(
            "Description: {}\n".format(
                data[class2index[class_id]]["description"]
            )
        )
        f.write("\n")
        for r in recommendations[class_id]:
            f.write(
                "{0:} : {1:.5f}\n".format(r["recommendation"], r["similarity"])
            )
            f.write(
                "Titre : {}\n".format(
                    data[class2index[r["recommendation"]]]["title"]
                )
            )
            f.write(
                "Description : {}\n".format(
                    data[class2index[r["recommendation"]]]["description"]
                )
            )
            f.write("\n")


###########################
# Preprocessing functions #
###########################


def preprocess(
    data: Data, hash_: str, data_dir: str
) -> Tuple[List[List[int]], List[List[str]], List[List[str]], str]:
    """
    Preprocesses the text data by tokenising and stemming it.

    Parameters
    ----------
    data : Data
        Data as returned by read_data
    hash_ : str
        Hash of the data
    data_dir : str
        Directory containing "PolyHEC" and where to put cache

    Returns
    -------
    List[List[int]]
        Final preprocessed text as indices after tokenising and stemming
    List[List[str]]
        Text after tokenising and stemming
    List[List[str]]
        Text after tokenising
    str
        Hash of the final preprocess text
    """
    start = time.time()
    pkl_path = os.path.join(data_dir, "cache", f"preprocessed_{hash_}.pkl")

    if os.path.exists(pkl_path):

        print("[*] Preprocessing: reading cache...".ljust(80), end="\r")
        with open(pkl_path, "rb") as f:
            preprocessed, stemmed, tokenised, hash_ = pickle.load(f)

    else:

        print("[*] Preprocessing: tokenising...".ljust(80), end="\r")
        tokenised = [
            tokenise(
                "{} {}".format(document["title"], document["description"])
            )
            for document in data
        ]

        print("[*] Preprocessing: stemming...".ljust(80), end="\r")
        stemmed = stem(tokenised)

        print(
            "[*] Preprocessing: converting to indices...".ljust(80), end="\r"
        )
        preprocessed, _ = to_indices(stemmed)

        print("[*] Preprocessing: hashing...".ljust(80), end="\r")
        hash_ = hash_data((preprocessed, stemmed, tokenised))

        print("[*] Preprocessing: caching...".ljust(80), end="\r")
        with open(pkl_path, "wb") as f:
            pickle.dump(
                (preprocessed, stemmed, tokenised, hash_),
                f,
                pickle.HIGHEST_PROTOCOL,
            )

    duration = time.time() - start

    print(f"[+] Preprocessing: done in {duration:<.2f} s.".ljust(80))
    return preprocessed, stemmed, tokenised, hash_


def tokenise(text: str) -> List[str]:
    """
    Tokenises the given text using french rules.

    Parameters
    ----------
    text : str
        Text to tokenise

    Returns
    -------
    List[str]
        Tokenised text
    """
    rules = [
        (r"(?:^\s+)|(?:\s+$)", ""),
        (r"(?<=[a-z])([^a-zA-Z\d_\-'])", r" \1"),
        (r"'", r"' "),
        (r"\"\b", '" '),
    ]

    def apply_rule(current, rule):
        return re.sub(rule[0], rule[1], current)

    text = text.lower()
    text = reduce(apply_rule, rules, text)
    return re.split(r"\s+", text)


def stem(text: List[List[str]]) -> List[List[str]]:
    """
    Stems the given text using the nltk French snowball stemmer.

    Parameters
    ----------
    text : List[str]
        Words to stem

    Returns
    -------
    List[str]
        Stemmed words
    """
    stemmer = FrenchStemmer()
    return [[stemmer.stem(word) for word in sentence] for sentence in text]


def to_indices(
    text: List[List[str]]
) -> Tuple[List[List[int]], Dict[str, int]]:
    """
    Converts the given text to indices and returns a dict to convert words to
    indices.

    Parameters
    ----------
    text : List[List[str]]
        Text to convert

    Returns
    -------
    List[List[int]]
        Converted text
    """
    vocab = set().union(*map(set, text))
    word2index = {w: i for i, w in enumerate(vocab)}
    return (
        [[word2index[word] for word in sentence] for sentence in text],
        word2index,
    )


#############################
# Text similarity functions #
#############################


def vectorize(
    preprocessed: List[List[int]],
    hash_: str,
    data_dir: str,
    vec_type: str,
    min_word_count: int,
    max_word_prop: float,
    max_vocab_size: int,
) -> Tuple[csr_matrix, str]:
    """
    Vectorises the given data.

    Parameters
    ----------
    preprocessed : List[List[int]]
        Preprocessed documents
    hash_ : str
        Hash of the inputs
    data_dir : str
        Directory containing "PolyHEC" and where to put cache
    vec_type : "bow" or "tfidf"
        Vectoriser type
    min_word_count : int
        Minimum occurence of word in documents
    max_word_prop : float
        Maximum frequency of word in documents
    max_vocab_size : int
        Maximum size of the vocabulary

    Returns
    -------
    csr_matrix of size n_documents x n_vocab
        Vectors of the documents
    str
        Hash of the outputs
    """
    start = time.time()
    pkl_path = os.path.join(
        data_dir,
        "cache",
        f"{vec_type}_{min_word_count}_{max_word_prop}_"
        f"{max_vocab_size}_{hash_}.pkl",
    )

    if os.path.exists(pkl_path):

        print("[*] Vectorizing: reading cache...".ljust(80), end="\r")
        with open(pkl_path, "rb") as f:
            vectorized, hash_ = pickle.load(f)

    else:

        print(
            "[*] Vectorizing: extracting vocabulary size...".ljust(80),
            end="\r",
        )
        n_vocab = max([xx for x in preprocessed for xx in x]) + 1

        print(
            "[*] Vectorising: computing term frequency...".ljust(80), end="\r"
        )
        tf = compute_term_frequency(preprocessed, n_vocab)

        print(
            "[*] Vectorising: computing document frequency...".ljust(80),
            end="\r",
        )
        df = compute_document_frequency(tf)

        print(
            "[*] Vectorising: applying frequency rules...".ljust(80), end="\r"
        )
        tf, df = apply_frequency_rules(
            tf, df, min_word_count, max_word_prop, max_vocab_size
        )

        if vec_type == "bow":
            vectorized = csr_matrix(tf)
        else:
            print("[*] Vectorising: computing TF-IDF...".ljust(80), end="\r")
            vectorized = compute_tfidf(tf, df)

        print("[*] Vectorising: hashing...".ljust(80), end="\r")
        hash_ = hash_data(vectorized)

        print("[*] Vectorising: caching...".ljust(80), end="\r")
        with open(pkl_path, "wb") as f:
            pickle.dump((vectorized, hash_), f, pickle.HIGHEST_PROTOCOL)

    duration = time.time() - start

    print(f"[+] Vectorising: done in {duration:<.2f} s.".ljust(80))
    return vectorized, hash_


def compute_term_frequency(
    preprocessed: List[List[int]], n_vocab: int
) -> csc_matrix:
    """Counts the occurence of each word in preprocessed.

    Parameters
    ----------
    preprocessed : List[List[int]]
        Preprocessed documents as word indices
    n_vocab: int
        Size of the vocabulary

    Returns
    -------
    csc_matrix of size n_documents x n_vocab
        Term frequency
    """
    counts = [
        {word: document.count(word) for word in set(document)}
        for document in preprocessed
    ]
    data = [count for document in counts for count in document.values()]
    cols = [col for document in counts for col in document.keys()]
    rows = [i for i, document in enumerate(counts) for _ in document]
    return csc_matrix((data, (rows, cols)), (len(preprocessed), n_vocab))


def compute_document_frequency(tf: csc_matrix) -> np.ndarray:
    """
    Counts the occurence of the presence of terms in documents.

    Parameters
    ----------
    tf : csc_matrix of size n_documents x n_out
        Term frequency

    Returns
    -------
    array-1 of size n_out
        Document frequency
    """
    return tf.getnnz(0)


def apply_frequency_rules(
    tf: csc_matrix,
    df: np.ndarray,
    min_word_count: int,
    max_word_prop: float,
    max_vocab_size: int,
) -> Tuple[csr_matrix, np.ndarray]:
    """
    Adjust the term frequency and document frequency with the given rules.

    Parameters
    ----------
    tf : csc_matrix of size n_documents x n_out
        Term frequency
    df : array-1 of size n_out
        Document frequency
    min_word_count : int
        Minimum number of expertise where a word appears for it to be kept
    max_word_prop : float
        Maximum proportion where a word can be for it to be kept
    max_vocab_size : int
        Maximum vocabulary size

    Returns
    -------
    csc_matrix of size n_documents x n_out
        Adjusted term frequency
    array-1 of size n_out
        Adjusted document frequency
    """
    n_documents = tf.shape[0]
    kept_indices = list(
        itertools.islice(
            map(
                itemgetter(0),
                sorted(
                    (
                        (i, count)
                        for i, count in enumerate(df)
                        if count >= min_word_count
                        and count / n_documents <= max_word_prop
                    ),
                    key=itemgetter(1),
                    reverse=True,
                ),
            ),
            max_vocab_size,
        )
    )

    tf = tf[:, kept_indices]
    df = df[kept_indices]

    return tf, df


def compute_tfidf(tf: csc_matrix, df: np.ndarray) -> csr_matrix:
    """
    Computes the TF-IDF matrix.

    Parameters
    ----------
    tf : csc_matrix of size n_documents x n_out
        Term frequency
    df : array-1 of size n_out
        Document frequency

    Returns
    -------
    csr_matrix of size n_documents x n_out
        TF-IDF
    """
    idf = np.log(tf.shape[0] / df)
    tfidf = csr_matrix(tf)
    tfidf.data = tfidf.data * np.take(idf, tfidf.indices)
    tfidf.eliminate_zeros()
    return tfidf


def reduce_matrix(
    matrix: csr_matrix,
    hash_: str,
    data_dir: str,
    n_reduced: int,
    norm_by: Optional[int] = None,
) -> Tuple[np.ndarray, str]:
    """
    Reduces the given matrix columns using SVD.

    Parameters
    ----------
    matrix : csr_matrix of size n_documents x n_vocab
        Matrix to reduce
    hash_ : str
        Hash of the inputs
    data_dir : str
        Directory containing "PolyHEC" and where to put cache
    n_reduced : int
        Size of the reduced matrix
    norm_by : Optional[int] (default: None)
        Axis on which to norm if wanted

    Returns
    -------
    array-2 of size n_documents x n_reduced
        Reduced matrix
    str
        Hash of the outputs
    """
    start = time.time()
    pkl_path = os.path.join(
        data_dir, "cache", f"reduced_{n_reduced}_{norm_by}_{hash_}.pkl"
    )

    if os.path.exists(pkl_path):

        print("[*] Reducing: reading cache...".ljust(80), end="\r")
        with open(pkl_path, "rb") as f:
            reduced, hash_ = pickle.load(f)

    else:
        print("[*] Reducing: computing SVD...".ljust(80), end="\r")
        U, D, _ = svds(matrix, n_reduced)
        reduced = U * D

        if norm_by is not None:
            load_print("Reducing: norming on axis {norm_by}")
            reduced = reduced / (
                np.linalg.norm(reduced, axis=norm_by, keepdims=True) + 1e-16
            )

        print("[*] Reducing: hashing...".ljust(80), end="\r")
        hash_ = hash_data(reduced)

        print("[*] Reducing: caching...".ljust(80), end="\r")
        with open(pkl_path, "wb") as f:
            pickle.dump((reduced, hash_), f, pickle.HIGHEST_PROTOCOL)

    duration = time.time() - start

    print(f"[+] Reducing: done in {duration:<.2f} s.".ljust(80))

    return reduced, hash_


############################
# Recommendation functions #
############################


def compute_recommendations(
    reduced: np.ndarray, idx: int, n_recommendations: int, normed: bool = False
) -> List[Recommendation]:
    """
    Computes the recommendations for the given row `idx`.

    Parameters
    ----------
    reduced : array-2 of size n_documents x k
        Reduced matrix
    idx : int
        Index of the row compared to
    n_recommendations : int
        Number of predictions to make
    normed : bool (default : False)
        If the reduced matrix is normed by row

    Returns
    -------
    List[Recommendation]
        Each element contains the "recommendation" and "similarity"
    """
    start = time.time()

    print("[*] Recommending: computing similarity...".ljust(80), end="\r")
    similiarities = compute_similarity(
        reduced, reduced[idx], matrix_normed=True, vector_normed=True
    )

    print("[*] Recommending: computing recommendations...".ljust(80), end="\r")
    predictions, similiarities = compute_predictions(
        similiarities, n_recommendations
    )
    recommendations = [
        {"recommendation": r, "similarity": s}
        for r, s in zip(predictions, similiarities)
    ]

    duration = time.time() - start

    print(f"[+] Recommending: done in {duration:<.2f} s.".ljust(80))
    return recommendations


def compute_similarity(
    matrix: np.ndarray,
    vector: np.ndarray,
    matrix_normed: bool = False,
    vector_normed: bool = False,
) -> np.ndarray:
    """
    Computes the cosine similarity between rows of `matrix` and `vector`.

    Parameters
    ----------
    matrix : array-2 of size n_documents x k
        Matrix containing all the data
    vector : array-1 of size k
        Vector to compare to
    matrix_normed : bool (default : False)
        If the matrix is normed by row
    vector_normed : bool (default : False)
        If the vector is normed

    Returns
    -------
    array-1 of size n_documents
        Similarities
    """
    if not matrix_normed:
        matrix = matrix / (
            np.linalg.norm(matrix, axis=1, keepdims=True) + 1e-16
        )
    if not vector_normed:
        vector = vector / (np.linalg.norm(vector) + 1e-16)
    return np.dot(matrix, vector)


def compute_predictions(
    similiarities: np.ndarray, n: int
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Compute the `n` best predictions given `similiarities`.

    Parameters
    ----------
    similiarities : array-1 of size n_documents
        Similarities
    n : int
        Number of predictions to make

    Returns
    -------
    predictions : array-1 of size n
        Predicted indices
    similiarities_ : array-1 of size n
        Corresponding similiarities
    """
    predictions = np.argpartition(-similiarities, n)[:n]
    similiarities_ = similiarities[predictions]

    sorting_indices = np.argsort(-similiarities_)
    predictions = predictions[sorting_indices]
    similiarities_ = similiarities_[sorting_indices]

    return predictions, similiarities_


#########
# Utils #
#########


def hash_data(data: Any) -> str:
    """
    Creates a hash from the data.

    Parameters
    ----------
    data : Any
        Structure to hash

    Returns
    -------
    str
        Hash
    """
    return hashlib.md5(pickle.dumps(data)).hexdigest()


def load_print(text: str) -> None:
    """
    Prints the text, formatting it to correspond to loading.
    """
    print(f"[*] {text}...".ljust(80), end="\r")


def done_print(text: str) -> None:
    """
    Prints the text, formatting it to correspond to done.
    """
    print(f"[+] {text}.".ljust(80))


if __name__ == "__main__":
    args = read_args()

    data, hash_ = read_data(args.data_dir, args.url)

    recommendations = recommend(
        args.class_,
        data,
        hash_,
        data_dir=args.data_dir,
        n_recommendations=args.n_recommendations,
        include_class=False,
        vec_type=args.vec_type,
        min_word_count=args.min_word,
        max_word_prop=args.max_prop,
        max_vocab_size=args.max_vocab,
        n_reduced=args.n_reduced,
    )

    write_result(recommendations, data, args.output)
