import mmap
import os
import re
import sys
from typing import List, Tuple

import pyphen  # type: ignore


def compute_flesch(text: str) -> Tuple[float, List[str], List[str], List[str]]:
    """
    Computes the Flesch Reading Ease for the given text using the formula:
    206.835
      - 1.015 (total_words / total_sentences)
      - 84.6 (total_syllables / total_words)

    Parameters
    ----------
    text : str
        Text to use

    Returns
    -------
    float
        Reading ease metric
    List[str]
        Text split in sentences
    List[str]
        Text split in words
    List[str]
        Text split in syllables
    """
    # compute metrics
    sentences = tokenise(text, on="sentences")
    words = tokenise(text, on="words")
    syllables = tokenise(text, on="syllables")
    total_sentences = len(sentences)
    total_words = len(words)
    total_syllables = len(syllables)
    # compute index
    flesch = (
        206.835
        - 1.015 * (total_words / total_sentences)
        - 84.6 * (total_syllables / total_words)
    )
    return flesch, sentences, words, syllables


def tokenise(text: str, on: str = "words") -> List[str]:
    """Tokenises the given text using french rules.

    Parameters
    ----------
    text : str
        Text to tokenise
    on : "syllables", "words", "sentences" (default : "words")
        How to tokenise

    Returns
    -------
    list of str
        Tokenised text
    """
    if on == "syllables":
        words = tokenise(text, "words")
        dic = pyphen.Pyphen(lang="en_US")
        tokenised = [
            syl
            for word in words
            for syl in dic.inserted(word).split("-")
            if syl != ""
        ]
    elif on == "words":
        # add space before non word chars
        text = re.sub(r"((?<!\.)[^a-zA-Z\d_\-])", r" \1", text)
        # change lone apostrophe after "s" to "'s"
        text = re.sub(r"(?<=s\s)'(?=\s)", "'s", text)
        # add space after quotes preceding a word
        text = re.sub(r"\"\b", '" ', text)
        # remove whitespace at start and end of text
        text = re.sub(r"(?:^\s+)|(?:\s+$)", "", text)
        tokenised = re.split(r"\s+", text)
    elif on == "sentences":
        tokenised = [
            sentence
            for sentence in re.split(r"(?<=[.!?])\s+", text)
            if sentence != ""
        ]
    else:
        raise RuntimeError(
            '`on` has to be one of "syllables", "words" or '
            f'"sentences"; is {on}.'
        )
    return tokenised


def analyze_file_lisibility(input_file: str) -> None:
    """
    Analyzes each line in the `input_file` using the flesch index and writes
    the results to the `output_file`.
    """
    if not os.path.exists(input_file):
        print("[!] Can't find input file.")
        sys.exit(1)
    output_file = input_file[:-4] + "_solution.txt"
    n_lines = count_lines(input_file)
    with open(input_file, "r") as infile:
        with open(output_file, "w") as outfile:
            for i in range(n_lines):
                print(
                    "Analyzing line {{0:>{}}}/{{1:}}".format(
                        len(str(n_lines))
                    ).format(i + 1, n_lines),
                    end="\r",
                )
                flesch, sentences, words, syllables = compute_flesch(
                    infile.readline().strip()
                )
                outfile.write(f"Line {i + 1}\n")
                outfile.write("-" * (5 + len(str(i + 1))) + "\n")
                outfile.write("Syllables:\n")
                outfile.write("-".join(syllables) + "\n")
                outfile.write("Total: {}\n\n".format(len(syllables)))
                outfile.write("Words:\n")
                outfile.write("-".join(words) + "\n")
                outfile.write("Total: {}\n\n".format(len(words)))
                outfile.write("Sentences:\n")
                outfile.write("-".join(sentences) + "\n")
                outfile.write("Total: {}\n\n".format(len(sentences)))
                outfile.write("Total: {:.3f}\n".format(flesch))
                if i != n_lines - 1:
                    outfile.write("\n\n")
            print()


def count_lines(filename):
    """Computes the number of lines in `filename`."""
    n = 0

    with open(filename, "r+") as f:
        buffer = mmap.mmap(f.fileno(), 0)
        while buffer.readline():
            n += 1

    return n


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("This script has to be used as :\n\tpython td1 file")
        sys.exit(1)
    input_file = sys.argv[1]
    analyze_file_lisibility(input_file)
