module Data.Info exposing (Info, Group)


type alias Info =
    { className : String
    , classSession : String
    , classNameEnglish : String
    , classSessionEnglish : String
    }


type alias Group =
    { number : String
    , day : String
    , time : String
    , local : String
    , teacherName : String
    , teacherEmail : String
    , teacherOffice : String
    }
