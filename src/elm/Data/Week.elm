module Data.Week exposing (Weeks, Week(..), Class, Slide, Chapter, Evaluation, Break)

import Date exposing (Date)


type Week
    = ClassWeek Class
    | BreakWeek Break


type alias Weeks =
    List Week


type alias Class =
    { english : ClassSingleLanguage
    , french : ClassSingleLanguage
    }


type alias ClassSingleLanguage =
    { week : Date
    , slides : List Slide
    , chapters : List Chapter
    , evaluations : List Evaluation
    , labs : List String
    }


type alias Slide =
    { text : String
    , url : String
    , englishUrl : String
    }


type alias Chapter =
    { text : String
    , url : String
    }


type alias Evaluation =
    { text : String
    , url : String
    , otherText : String
    }


type alias Break =
    { english : BreakSingleLanguage
    , french : BreakSingleLanguage
    }


type alias BreakSingleLanguage =
    { name : String
    , week : Date
    }
