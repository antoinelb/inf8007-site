module Data.Exercises exposing (AllExercises, Exercises, Exercise, ExerciseType(..))


type alias AllExercises =
    { comprehensions : Exercises
    , regularExpressions : Exercises
    , tests : Exercises
    , numpy : Exercises
    , servers : Exercises
    , frontend : Exercises
    , functionalProgramming : Exercises
    , dataVisualisation : Exercises
    }


type ExerciseType
    = ComprehensionsType
    | RegularExpressionsType
    | TestsType
    | NumpyType
    | ServersType
    | FrontendType
    | FunctionalProgrammingType
    | DataVisualisationType


type alias Exercises =
    List Exercise


type alias Exercise =
    { question : String
    , answer : String
    , questionEnglish : String
    , answerEnglish : String
    , showingAnswer : Bool
    }
