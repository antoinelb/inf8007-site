module Data.Language exposing (Language(..))


type Language
    = English
    | French
