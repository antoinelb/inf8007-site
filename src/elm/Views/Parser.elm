module Views.Parser exposing (parseText)

import Html exposing (..)
import Html.Attributes exposing (..)
import Regex
import KaTeX exposing (render, renderToString, renderWithOptions, defaultOptions)


parseText : String -> List (Html msg)
parseText text_ =
    let
        parseMath i t =
            if i % 2 == 0 then
                toNewLines (splitNewLine t)
            else
                [ toMath t ]

        parseCode i t =
            if i % 2 == 0 then
                List.concat <| List.indexedMap parseMath (splitMath <| Tuple.first t)
            else
                [ toCode t ]
    in
        List.concat <| List.indexedMap parseCode (splitCode text_)


splitCode : String -> List ( String, String )
splitCode text_ =
    let
        split =
            Regex.split Regex.All (Regex.regex "</?code[^>]*>") text_

        langMatch =
            Regex.find Regex.All (Regex.regex "<code(?: lang=\"(\\w+)\")?>") text_

        extractLang match =
            if String.contains "lang" match.match then
                String.slice 12 -2 match.match
            else
                "python"

        lang__ =
            List.concat
                [ [ "python" ]
                , List.intersperse "python" (List.map extractLang langMatch)
                ]

        lang_ =
            if (List.length split) /= (List.length lang__) then
                List.concat [ lang__, [ "python" ] ]
            else
                lang__
    in
        List.map2 (,) split lang_


splitNewLine : String -> List String
splitNewLine text_ =
    String.split "\n" text_


splitMath : String -> List String
splitMath text_ =
    let
        split =
            Regex.split Regex.All (Regex.regex "(?!\\\\)\\$") text_
    in
        List.map (\t -> Regex.replace Regex.All (Regex.regex "\\\\\\$") (\_ -> "$") t) split


toText : String -> Html msg
toText text_ =
    text text_


toTabbedText : String -> Html msg
toTabbedText text_ =
    let
        tabSize =
            toString <| 4 * (List.length <| Regex.find Regex.All (Regex.regex "\\t") text_)
    in
        span [ style [ ( "padding-left", tabSize ++ "ch" ) ] ] [ text text_ ]


toNewLines : List String -> List (Html msg)
toNewLines split_text =
    List.intersperse (br [] []) (List.map toText split_text)


toCode : ( String, String ) -> Html msg
toCode text_lang =
    let
        text_ =
            Tuple.first text_lang

        lang_ =
            Tuple.second text_lang

        withNewLines =
            splitNewLine text_

        withTabs =
            List.map toTabbedText withNewLines

        inCode =
            List.intersperse (br [] []) withTabs
    in
        code [ lang lang_ ] inCode


toMath : String -> Html msg
toMath text_ =
    renderWithOptions { defaultOptions | displayMode = True } text_
