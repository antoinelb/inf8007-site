module Views.Menu exposing (menu, menuEnglish)

import Html exposing (Html, a, div, text)
import Html.Attributes exposing (class, id)
import Route exposing (Route, href)


menu : Html msg
menu =
    div [ id "menu" ]
        [ a [ href Route.Home, class "menu-item" ] [ text "Accueil" ]
        , a [ href Route.Weeks, class "menu-item" ] [ text "Séances" ]
        , a [ href Route.Plan, class "menu-item" ] [ text "Plan" ]
        , a [ href Route.TD1, class "menu-item" ] [ text "TD1" ]
        , a [ href Route.TD2, class "menu-item" ] [ text "TD2" ]
        , a [ href Route.TD3, class "menu-item" ] [ text "TD3" ]
        , a [ href (Route.Exercises Nothing), class "menu-item" ] [ text "Exercices" ]
        ]


menuEnglish : Html msg
menuEnglish =
    div [ id "menu" ]
        [ a [ href Route.Home, class "menu-item" ] [ text "Home" ]
        , a [ href Route.Weeks, class "menu-item" ] [ text "Classes" ]
        , a [ href Route.Plan, class "menu-item" ] [ text "Plan" ]
        , a [ href Route.TD1, class "menu-item" ] [ text "TD1" ]
        , a [ href Route.TD2, class "menu-item" ] [ text "TD2" ]
        , a [ href Route.TD3, class "menu-item" ] [ text "TD3" ]
        , a [ href (Route.Exercises Nothing), class "menu-item" ] [ text "Exercises" ]
        ]
