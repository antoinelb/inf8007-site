module Config exposing (classInfo, classGroups, labGroups, weeks, lastUpdate, exercises)

import Data.Info exposing (Info, Group)
import Data.Week exposing (Weeks, Week(..), Class, Slide, Chapter, Evaluation, Break)
import Data.Exercises exposing (AllExercises, Exercises, Exercise)
import Date
import Result


-- TO CONFIGURE
-- CLASS INFO


lastUpdate : Date.Date
lastUpdate =
    Date.fromString "2019-02-18 09:56" |> Result.withDefault (Date.fromTime 0)


classInfo : Info
classInfo =
    { className = "INF8007 - Languages de script"
    , classSession = "Hiver 2019"
    , classNameEnglish = "INF8007 - Scripting Languages"
    , classSessionEnglish = "Winter 2019"
    }


classGroups : List Group
classGroups =
    [ lecture1 ]


labGroups : List Group
labGroups =
    [ lab1 ]


lecture1 : Group
lecture1 =
    { number = "01"
    , day = "Lundi"
    , time = "8h30-10h20"
    , local = "A-526"
    , teacherName = "Antoine Lefebvre-Brossard"
    , teacherEmail = "antoine.lefebvre-brossard@polymtl(point)ca"
    , teacherOffice = "M-4209"
    }


lab1 : Group
lab1 =
    { number = "01"
    , day = "Lundi"
    , time = "10h30-12h20"
    , local = "L-3712"
    , teacherName = "Antoine Lefebvre-Brossard"
    , teacherEmail = "antoine.lefebvre-brossard@polymtl(point)ca"
    , teacherOffice = "M-4209"
    }



-- WEEK INFO


weeks : Weeks
weeks =
    [ ClassWeek class1
    , ClassWeek class2
    , ClassWeek class3
    , ClassWeek class4
    , ClassWeek class5
    , ClassWeek class6
    , ClassWeek class7
    , ClassWeek class8
    ]


class1 : Class
class1 =
    { english =
        { week = Date.fromString "2019-01-07" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Introduction" "introduction.pdf" "introduction_en.pdf")
            , (Slide "Basic syntax" "syntaxe_de_base.pdf" "syntaxe_de_base_en.pdf")
            ]
        , chapters =
            [ (Chapter "0" "http://www.diveintopython3.net/installing-python.html")
            , (Chapter "1" "http://www.diveintopython3.net/your-first-python-program.html")
            , (Chapter "2" "http://www.diveintopython3.net/native-datatypes.html")
            ]
        , evaluations = []
        , labs = [ "TD1" ]
        }
    , french =
        { week = Date.fromString "2019-01-07" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Introduction" "introduction.pdf" "introduction_en.pdf")
            , (Slide "Syntaxe de base" "syntaxe_de_base.pdf" "syntaxe_de_base_en.pdf")
            ]
        , chapters =
            [ (Chapter "0" "http://www.diveintopython3.net/installing-python.html")
            , (Chapter "1" "http://www.diveintopython3.net/your-first-python-program.html")
            , (Chapter "2" "http://www.diveintopython3.net/native-datatypes.html")
            ]
        , evaluations = []
        , labs = [ "TD1" ]
        }
    }


class2 : Class
class2 =
    { english =
        { week = Date.fromString "2019-01-14" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Regular expressions" "expressions_regulieres.pdf" "expressions_regulieres_en.pdf")
            , (Slide "Comprehensions" "comprehensions.pdf" "comprehensions_en.pdf")
            ]
        , chapters =
            [ (Chapter "3" "http://www.diveintopython3.net/comprehensions.html")
            , (Chapter "4" "http://www.diveintopython3.net/strings.html")
            , (Chapter "5" "http://www.diveintopython3.net/regular-expressions.html")
            , (Chapter "HackerRank" "https://www.hackerrank.com/")
            ]
        , evaluations = [ (Evaluation "TD1" "#td1" " (To submit before Jan-18 23:55 )") ]
        , labs = [ "TD1 (continued)" ]
        }
    , french =
        { week = Date.fromString "2019-01-14" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Expressions régulières" "expressions_regulieres.pdf" "expressions_regulieres_en.pdf")
            , (Slide "Compréhensions" "comprehensions.pdf" "comprehensions_en.pdf")
            ]
        , chapters =
            [ (Chapter "3" "http://www.diveintopython3.net/comprehensions.html")
            , (Chapter "4" "http://www.diveintopython3.net/strings.html")
            , (Chapter "5" "http://www.diveintopython3.net/regular-expressions.html")
            , (Chapter "HackerRank" "https://www.hackerrank.com/")
            ]
        , evaluations = [ (Evaluation "TD1" "#td1" " (À remttre avant le Jan-18 23:55 )") ]
        , labs = [ "TD1 (suite)" ]
        }
    }


class3 : Class
class3 =
    { english =
        { week = Date.fromString "2019-01-21" |> Result.withDefault (Date.fromTime 0)
        , slides =
          [
             (Slide "File handling" "gestion_de_fichiers.pdf" "gestion_de_fichiers_en.pdf")
            , (Slide "Numpy" "numpy.pdf" "numpy_en.pdf")
            ]
        , chapters =
            [ (Chapter "9" "http://www.diveintopython3.net/unit-testing.html")
            ]
        , evaluations = []
        , labs = [ "TD2" ]
        }
    , french =
        { week = Date.fromString "2019-01-21" |> Result.withDefault (Date.fromTime 0)
        , slides =
          [
             (Slide "Gestion de fichiers" "gestion_de_fichiers.pdf" "gestion_de_fichiers_en.pdf")
            , (Slide "Numpy" "numpy.pdf" "numpy_en.pdf")
            ]
        , chapters =
            [ (Chapter "9" "http://www.diveintopython3.net/unit-testing.html")
            ]
        , evaluations = []
        , labs = [ "TD2" ]
        }
    }


class4 : Class
class4 =
    { english =
        { week = Date.fromString "2019-01-28" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Tests" "tests.pdf" "tests_en.pdf")
            , (Slide "Text similarity" "similarite_de_textes.pdf" "similarite_de_textes_en.pdf")
            ]
        , chapters =
            []
        , evaluations = []
        , labs = [ "TD2 (continued)" ]
        }
    , french =
        { week = Date.fromString "2019-01-28" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Tests" "tests.pdf" "tests_en.pdf")
            , (Slide "Similarité de textes" "similarite_de_textes.pdf" "similarite_de_textes_en.pdf")
            ]
        , chapters =
            []
        , evaluations = []
        , labs = [ "TD2 (suite)" ]
        }
    }


class5 : Class
class5 =
    { english =
        { week = Date.fromString "2019-02-04" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Serveurs" "serveurs.pdf" "serveurs_en.pdf")
             ,(Slide "Frontend" "frontend.pdf" "frontend_en.pdf") ]
        , chapters =
            [ (Chapter "12" "http://www.diveintopython3.net/xml.html") ]
        , evaluations = [ (Evaluation "TD2" "#td2" " (To submit before Fev-15 23:55 )") ]
        , labs = [ "TD2 (continued)" ]
        }
    , french =
        { week = Date.fromString "2019-02-04" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Servers" "serveurs.pdf" "serveurs_en.pdf") 
            , (Slide "Frontend" "frontend.pdf" "frontend_en.pdf") ]
        , chapters =
            [ (Chapter "12" "http://www.diveintopython3.net/xml.html") ]
        , evaluations = [ (Evaluation "TD2" "#td2" " (À remettre avant le Fev-15 23:55 )") ]
        , labs = [ "TD2 (suite)" ]
        }
    }


class6 : Class
class6 =
    { english =
        { week = Date.fromString "2019-02-11" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Functional programming" "programmation_fonctionnelle.pdf" "programmation_fonctionnelle_en.pdf")
            ]
        , chapters =
            []
        , evaluations = []
        , labs = [ "TD3" ]
        }
    , french =
        { week = Date.fromString "2019-02-11" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Programmation fonctionnelle" "programmation_fonctionnelle.pdf" "programmation_fonctionnelle_en.pdf")
            ]
        , chapters =
            []
        , evaluations = []
        , labs = [ "TD3" ]
        }
    }


class7 : Class
class7 =
    { english =
        { week = Date.fromString "2019-02-18" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Revision" "revision.pdf" "revision_en.pdf") ]
        , chapters =
            []
        , evaluations = [ (Evaluation "TD3" "#td3" " (To submit before Fev-22 23:55 )") ]
        , labs = [ "TD3 (continued)" ]
        }
    , french =
        { week = Date.fromString "2019-02-18" |> Result.withDefault (Date.fromTime 0)
        , slides =
            [ (Slide "Révision" "revision.pdf" "revision_en.pdf") ]
        , chapters =
            []
        , evaluations = [ (Evaluation "TD3" "#td3" " (À remettre avant le Fev-22 23:55 )") ]
        , labs = [ "TD3 (suite)" ]
        }
    }


class8 : Class
class8 =
    { english =
        { week = Date.fromString "2019-02-25" |> Result.withDefault (Date.fromTime 0)
        , slides =
            []
        , chapters =
            []
        , evaluations = [ Evaluation "Final exam" "" "" ]
        , labs = []
        }
    , french =
        { week = Date.fromString "2019-02-25" |> Result.withDefault (Date.fromTime 0)
        , slides =
            []
        , chapters =
            []
        , evaluations = [ Evaluation "Examen final" "" "" ]
        , labs = []
        }
    }


break1 : Break
break1 =
    { english =
        { name = "Spring break"
        , week = Date.fromString "2018-03-06" |> Result.withDefault (Date.fromTime 0)
        }
    , french =
        { name = "Semaine de relâche"
        , week = Date.fromString "2018-03-06" |> Result.withDefault (Date.fromTime 0)
        }
    }



-- EXERCISE INFO


exercises : AllExercises
exercises =
    { comprehensions = comprehensionsExercises
    , regularExpressions = regularExpressionsExercises
    , tests = testsExercises
    , numpy = numpyExercises
    , servers = serversExertices
    , frontend = frontendExertices
    , functionalProgramming = functionalProgrammingExercises
    , dataVisualisation = dataVisualisationExercises
    }


comprehensionsExercises : Exercises
comprehensionsExercises =
    [ (Exercise
        "Créer une liste avec toutes les combinaisons possibles faites à partir de 2 dés (l'ordre est important)."
        "<code>[(i, j) for j in range(1, 7) for i in range(1, 7)]</code>"
        "Create a list with all possible combinations from 2 die (the order matters)."
        "<code>[(i, j) for j in range(1, 7) for i in range(1, 7)]</code>"
        False
      )
    , (Exercise
        "Créer un dictionnaire avec les fréquences des sommes possibles de 3 dés."
        "<code>counts = [i + j + k for k in range(1, 7) for j in range(1, 7) for k in range(1, 7)] \n frequencies = {s: counts.count(s) for s in set(counts)}</code>"
        "Create a dictionary with the frequencies of all possible sums of 3 die."
        "<code>counts = [i + j + k for k in range(1, 7) for j in range(1, 7) for k in range(1, 7)] \n frequencies = {s: counts.count(s) for s in set(counts)}</code>"
        False
      )
    , (Exercise
        "Calculer la moyenne et la variance de \n <code>data = [28.7084756, 14.06263029, 21.24275088, 19.50508908, 12.33448849, 11.30270102, 25.55622922, 18.19997674, 25.83619107, 31.3355505, 19.60778894, 25.2276457, 10.08244735, 22.31033544, 19.74242935, 31.85747843, 20.15679573, 19.80501122, 22.72908789, 20.85818683]</code>"
        "<code>mean = sum(data) / len(data) \n var = sum((x - mean)**2 for x in data) / len(data)</code>"
        "Compute the mean and variance of \n <code>data = [28.7084756, 14.06263029, 21.24275088, 19.50508908, 12.33448849, 11.30270102, 25.55622922, 18.19997674, 25.83619107, 31.3355505, 19.60778894, 25.2276457, 10.08244735, 22.31033544, 19.74242935, 31.85747843, 20.15679573, 19.80501122, 22.72908789, 20.85818683]</code>"
        "<code>mean = sum(data) / len(data) \n var = sum((x - mean)**2 for x in data) / len(data)</code>"
        False
      )
    , (Exercise
        "Extraire le vocabulaire des phrases suivantes : \n \"The quick brown fox\" \n \"The fox jumps over\" \n \"The lazy dog jumps\""
        "<code>vocab = set().union(*(set(s.lower().split()) for s in sentences))</code>"
        "Extract the vocabulary from the following sentences: \n \"The quick brown fox\" \n \"The fox jumps over\" \n \"The lazy dog jumps\""
        "<code>vocab = set().union(*(set(s.lower().split()) for s in sentences))</code>"
        False
      )
    , (Exercise
        "Convertir chacune des phrases suivantes en un index de ces mots (il faut créer cet index): \n \"The quick brown fox\" \n \"The fox jumps over\" \n \"The lazy dog jumps\""
        "<code>vocab = set().union(*(set(s.lower().split()) for s in sentences)) \n word2index = {w: i for i, w in enumerate(vocab)} \n indices = [[word2index[w.lower()] for w in s.split()] for s in sentences]</code>"
        "Convert each of the following sentences into an index of its words (the index has to be created): \n \"The quick brown fox\" \n \"The fox jumps over\" \n \"The lazy dog jumps\""
        "<code>vocab = set().union(*(set(s.lower().split()) for s in sentences)) \n word2index = {w: i for i, w in enumerate(vocab)} \n indices = [[word2index[w.lower()] for w in s.split()] for s in sentences]</code>"
        False
      )
    ]


regularExpressionsExercises : Exercises
regularExpressionsExercises =
    [ (Exercise
        "Écrire une expression régulière pour valider une adresse courriel qui matche les 2 premières addresses, mais pas les 2 suivantes : \n \"firstname.lastname@polymtl.ca\" \n \"w€ird.adre\\$\\$.with-\\$ymbols@someprovider.com\" \n \"notvalidaddress1@provider.com.ca\" \n \"notvalid@dress2@provider.com\""
        "<code>r\"^[^\\@]+@[a-z]+\\.[a-z]+$\"</code>"
        "Create a regular expression to validate email adresses and make sure it matches the first 2, but not the last 2 : \n \"firstname.lastname@polymtl.ca\" \n \"w€ird.adre$$.with-$ymbols@someprovider.com\" \n \"notvalidaddress1@provider.com.ca\" \n \"notvalid@dress2@provider.com\""
        "<code>r\"^[^\\@]+@[a-z]+\\.[a-z]+$\"</code>"
        False
      )
    , (Exercise
        "Écrire une expression régulière pour valider un numéro de téléphone dans une des formes suivantes : \n \"1 (xxx) xxx-xxxx\" \n \"(xxx) xxx-xxxx\" \n \"xxx-xxx-xxxx\" \n \"xxx-xxxx\""
        "<code>r\"^(1\\s)?(\\(\\d{3}\\)\\s|\\d{3}-)?\\d{3}-\\d{4}$\"</code>"
        "Write a regular expression to validate any phone number in either of these forms : \n \"1 (xxx) xxx-xxxx\" \n \"(xxx) xxx-xxxx\" \n \"xxx-xxx-xxxx\" \n \"xxx-xxxx\""
        "<code>r\"^(1\\s)?(\\(\\d{3}\\)\\s|\\d{3}-)?\\d{3}-\\d{4}$\"</code>"
        False
      )
    , (Exercise
        "Écrire une expression régulière pouvant extraire le texte de n'importe quel tag HTML en prenant comme exemples les suivants : \n \"<p>text</p>\" \n \"<a href='link'>text</a>\" \n \"<p><span>text</span></p>\""
        "<code>r\"^(?:<[^>]+>)+([^<]+)(?:</[^>]+>)+$\"</code>"
        "Write a regular expression able to extract text from any html tag taking as exemples the following : \n \"<p>text</p>\" \n \"<a href='link'>text</a>\" \n \"<p><span>text</span></p>\""
        "<code>r\"^(?:<[^>]+>)+([^<]+)(?:</[^>]+>)+$\"</code>"
        False
      )
    , (Exercise
        "Écrire une expression régulière pouvant extraire les noms propres d'un texte en prenant comme exemples (les mots de début de phrase peut être gardés puisqu'ils peuvent facilement retirés par la suite si nécessaire) : \n \"Trouver moi Napoléon Buonaparte!\" \n \"Québec considère Montréal comme sa grande rivale. Montréal se rappelle parfois que Québec existe.\" \n \"Cosette fut confiée à Jean Valjean. Celui-ci avait peur de Mr. Javert.\""
        "<code>r\"((?:(?:[A-Z][a-z]\\.\\s)?\\b[A-Z][a-zéèêàç]+\\b\\s?)+)\"</code>"
        "Write a regular expression to extract the names from a text taking as exemple the following (words at the start of the sentence may be kept since they're easy to get rid of if necessary) : \n \"Trouver moi Napoléon Buonaparte!\" \n \"Québec considère Montréal comme sa grande rivale. Montréal se rappelle parfois que Québec existe.\" \n \"Cosette fut confiée à Jean Valjean. Celui-ci avait peur de Mr. Javert.\""
        "<code>r\"((?:(?:[A-Z][a-z]\\.\\s)?\\b[A-Z][a-zéèêàç]+\\b\\s?)+)\"</code>"
        False
      )
    ]


testsExercises : Exercises
testsExercises =
    [ (Exercise
        "Quelle serait une fonction correspondant aux tests suivants?\n<code>assert fct(2017) == 365\nassert fct(2016) == 366\nassert fct(2000) == 366\nassert fct(1900) == 365\nassert fct(2015) == 365\nassert fct(1997) == 365\nassert fct(1992) == 366</code>"
        "<code>def days_in_year(year: int) -> int:\n\tif year % 4 != 0:\n\t\treturn 365\n\telif year % 100 != 0:\n\t\treturn 366\n\telif year % 400 == 0:\n\t\treturn 366\n\telse:\n\t\treturn 365</code>"
        "What would be a function corresponding to the following tests?\n<code>assert fct(2017) == 365\nassert fct(2016) == 366\nassert fct(2000) == 366\nassert fct(1900) == 365\nassert fct(2015) == 365\nassert fct(1997) == 365\nassert fct(1992) == 366</code>"
        "<code>def days_in_year(year: int) -> int:\n\tif year % 4 != 0:\n\t\treturn 365\n\telif year % 100 != 0:\n\t\treturn 366\n\telif year % 400 == 0:\n\t\treturn 366\n\telse:\n\t\treturn 365</code>"
        False
      )
    , (Exercise
        "Quels seraient des tests pour la fonction suivante?\n<code>def find_bit_size(n: int) -> int:\n\tk = 1\n\twhile 2**k <= n:\n\t\tk = k + 1\n\treturn k</code>"
        "<code>assert fct(0) == 1\nassert fct(1) == 1\nassert fct(2) == 2\nassert fct(15) == 4\nassert fct(2000) == 11"
        "What would be tests for the folliwing function?\n<code>def find_bit_size(n: int) -> int:\n\tk = 0\n\twhile 2**k <= n:\n\t\tk = k + 1\n\treturn k</code>"
        "<code>assert fct(0) == 1\nassert fct(1) == 1\nassert fct(2) == 2\nassert fct(15) == 4\nassert fct(2000) == 11"
        False
      )
    , (Exercise
        "Est-ce que la fonction suivante fonctionne avec les tests?\n<code>def pad(text: str, n: int) -> str:\n\ttext = \"X\" * (n - len(text))\n\treturn text\ns1 = \"the quick brown fox\"\ns2 = \"jumps over the lazy dog\"\nassert pad(\"cat\", 10) == \"catXXXXXXXX\nassert \"\".join(pad(x, 5) for x in s1.split()) == \"XXXthe quick brown XXfox\"code>/code>\nassert \"\".join(pad(x, max(map(len, s2.split()))) for x in s2.split() == \"Xjumps XXover XXthe XXlazy XXdog</code>"
        "Non\nOui\nNon"
        "Will the following function and tests work together?\n<code>code>def pad(text: str, n: int) -> str:\n\ttext = \"X\" * (n - len(text))\n\treturn text\ns1 = \"the quick brown fox\"\ns2 = \"jumps over the lazy dog\"\nassert pad(\"cat\", 10) == \"catXXXXXXXX\nassert \"\".join(pad(x, 5) for x in s1.split()) == \"XXXthe quick brown XXfox\"code>/code>\nassert \"\".join(pad(x, max(map(len, s2.split()))) for x in s2.split() == \"Xjumps XXover XXthe XXlazy XXdog</code>"
        "No\nYes\nNo"
        False
      )
    ]


numpyExercises : Exercises
numpyExercises =
    [ (Exercise
        "Écrire une fonction avec numpy pour retourner la corrélation entre des vecteurs X et Y\n$\\frac{E[XY] - E[X]E[Y]}{\\sqrt{E[X^2] - E[X]^2}\\sqrt{E[Y^2] - E[Y]^2}}$ où E[X] est la moyenne (espérance) de X."
        "<code>import numpy as np\ndef correlation(X: np.ndarray, Y: np.ndarray) -> float:\n\te_X = X.mean()\n\te_X2 = (X**2).mean()\n\te_Y = Y.mean()\n\te_Y2 = (Y**2).mean()\n\te_XY = (X * Y).mean()\n\tcorr = (e_XY - e_X * e_Y) / (np.sqrt(e_X2 - e_X**2) * np.sqrt(e_Y2 - e_Y**2))\n\treturn corr</code>"
        "Write a function using numpy to return the correlation between vectors X and Y \n $\\frac{E[XY] - E[X]E[Y]}{\\sqrt{E[X^2] - E[X]^2}\\sqrt{E[Y^2] - E[Y]^2}}$ \n where E[X] is the mean (expectation) of X."
        "<code>import numpy as np\ndef correlation(X: np.ndarray, Y: np.ndarray) -> float:\n\te_X = X.mean()\n\te_X2 = (X**2).mean()\n\te_Y = Y.mean()\n\te_Y2 = (Y**2).mean()\n\te_XY = (X * Y).mean()\n\tcorr = (e_XY - e_X * e_Y) / (np.sqrt(e_X2 - e_X**2) * np.sqrt(e_Y2 - e_Y**2))\n\treturn corr</code>"
        False
      )
    , (Exercise
        "Écrire une fonction qui retourne une valeur aléatoire issu d'une distribution normale en utilisant la fonction <code>np.random.rand</code> et des valeurs arbitraires pour la moyenne et la variance.\nUtiliser le fait que\n$r = F(x)$\noù r provient d'une distribution uniforme (<code>np.random.rand</code>) et F est la fonction de répartition de la normale. Cette fonction de répartition peut être prise de <code>scipy.stats.norm.cdf</code>."
        "<code>import numpy as np\nimport scipy as sp\ndef random_normal(mu: float, sigma2: float) -> float:\n\tr = np.random.rand()\n\tx_standard = sp.stats.norm.cdf(r)\n\tx = x_standard * np.sqrt(sigma2) + mu\n\treturn x</code>"
        "Write a function that returns a random value in a normal distribution using only the function <code>np.random.rand</code> and arbitrary measures for the mean and variance.\nUse the fact that \n$r = F(x)$\nwhere r comes from a random uniform (<code>np.random.rand</code>) and F and is the normal cumulative function. This cumulative function can be obtained from <code>scipy.stats.norm.cdf</code>."
        "<code>import numpy as np\nimport scipy as sp\ndef random_normal(mu: float, sigma2: float) -> float:\n\tr = np.random.rand()\n\tx_standard = sp.stats.norm.cdf(r)\n\tx = x_standard * np.sqrt(sigma2) + mu\n\treturn x</code>"
        False
      )
    , (Exercise
        "Quelle sera la forme de X à la fin du code suivant?\n<code>X = np.random.random([3, 4, 5])\nX = np.dot(X, np.random.random([5, 7]))\nX = X.reshape([3, -1])\nX = X + np.random.random([1, X.shape[-1]])\nX = X.sum(axis=0)\nprint(X.shape)</code>"
        "(28,)"
        "What will be the shape of X at the end of the following code?\n<code>X = np.random.random([3, 4, 5])\nX = np.dot(X, np.random.random([5, 7]))\nX = X.reshape([3, -1])\nX = X + np.random.random([1, X.shape[-1]])\nX = X.sum(axis=0)\nprint(X.shape)</code>"
        "(28,)"
        False
      )
    ]


serversExertices : Exercises
serversExertices =
    [ (Exercise
        "Un développeur est en charge d'écrire l'API pour la modification du mot de passe. Le serveur devrait-il accepter les requêtes GET, POST ou les deux et pourquoi?"
        "Il devrait utiliser un POST puisque les mots de passe sont des informations sensibles et qu'ils ne devraient jamais pouvoir être vus."
        "A developer is in charge of writing the API for the password modification. Should the server accept GET, POST or both requests and why?"
        "He should use a POST since passwords are sensitive information and should never be seen."
        False
      )
    , (Exercise
        "Vous êtes en charge de créer l'API pour une bibliothèque de la ville de Montréal. La bibliothécaire responsable voudrait avoir accès aux informations des livres contenus dans le base de données du format suivant:\n<code>data = [{\"ISBN\": str, \"Title\": str, \"Author\": str, \"Borrowed\": bool, \"Reserved\": bool}]</code>\nVous pouvez utiliser le framework de votre choix."
        "<code>app = Sanic()\ndata = read_data()\ndef books_handler(req: Request) -> HTTPResponse:\n\t_data = data\n\tif \"isbn\" in req.args:\n\t\t_data = [x for x in _data if x[\"ISBN\"] == req.raw_args[\"isbn\"]]\n\tif \"title\" in req.args:\n\t\t_data = [x for x in _data if x[\"Title\"] == req.raw_args[\"title\"]]\n\tif \"author\" in req.args:\n\t\t_data = [x for x in _data if x[\"Author\"] == req.raw_args[\"author\"]]\n\treturn json(_data, status=200)\napp.add_route(books_handler, \"/books\", methods=[\"GET\"])\napp.add_route(books_handler, r\"/books?isbn=<isbn:\\d{3}-\\d{2}-\\d-\\d{6}-\\d>\", methods=[\"GET\"])\napp.add_route(books_handler, r\"/books?title=<title:str>\", methods=[\"GET\"])\napp.add_route(books_handler, r\"/books?author=<author:str>\", methods=[\"GET\"])</code>"
        "You are in charge of creating the API for a Montreal library. The librarian in charge wants to have access to the information of books contained in the database of the following schema:\n<code>data = [{\"ISBN\": str, \"Title\": str, \"Author\": str, \"Borrowed\": bool, \"Reserved\": bool}]</code>\nYou may use the framework of your choice."
        "<code>app = Sanic()\ndata = read_data()\ndef books_handler(req: Request) -> HTTPResponse:\n\t_data = data\n\tif \"isbn\" in req.args:\n\t\t_data = [x for x in _data if x[\"ISBN\"] == req.raw_args[\"isbn\"]]\n\tif \"title\" in req.args:\n\t\t_data = [x for x in _data if x[\"Title\"] == req.raw_args[\"title\"]]\n\tif \"author\" in req.args:\n\t\t_data = [x for x in _data if x[\"Author\"] == req.raw_args[\"author\"]]\n\treturn json(_data, status=200)\napp.add_route(books_handler, \"/books\", methods=[\"GET\"])\napp.add_route(books_handler, r\"/books?isbn=<isbn:\\d{3}-\\d{2}-\\d-\\d{6}-\\d>\", methods=[\"GET\"])\napp.add_route(books_handler, r\"/books?title=<title:str>\", methods=[\"GET\"])\napp.add_route(books_handler, r\"/books?author=<author:str>\", methods=[\"GET\"])</code>"
        False
      )
    , (Exercise
        "La bibliothécaire a tellement apprécié vos services qu'elle vous a recommendé aux piscines de Montréal. Celles-ci voudraient que vous créiez le API pour accéder aux informations suivantes: les informations des piscines, les informations des sauveteurs et les informations des mainteneurs de piscine. De plus elles voudraients que chaque piscine puisse accéder seulement à ses propres informations. Chacun des employés voudraient faire la même chose. Écrivez seulement les routes qui permettraient de récupérer toutes ces informations."
        "<code>\"/pools/\"\n\"/pools?name=<name:str>\"\n\"/lifeguards\"\n\"/lifeguards?employee_id=<employee_id:int>\"\n\"/mainteners\"\n\"/mainteners?employee_id=<employee_id:int>\"</code>"
        "The librarian thought you did such a great job that she recommended you to the pools of Montreal. They would like you to create an API to get access to the following information: data on all pools, data on all lifeguards and data on all pool mainteners. Each pool would also like to be able to access only its information as well as each employee for its own info. Only write the routes to get access to this data."
        "<code>\"/pools/\"\n\"/pools?name=<name:str>\"\n\"/lifeguards\"\n\"/lifeguards?employee_id=<employee_id:int>\"\n\"/mainteners\"\n\"/mainteners?employee_id=<employee_id:int>\"</code>"
        False
      )
    ]


frontendExertices : Exercises
frontendExertices =
    [ (Exercise
        "Vous avez trouvé que le concept de la réponse qui ne s'affiche que quand on clique sur un bouton très utile sur la page web du cours et vous désirez maintenant le répliquer. Écrivez le code HTML, CSS et JavaScript pour le faire."
        "index.html:\n<code lang=\"html\"><html>\n<head>\n\t<link href=\"style.css\" rel=\"stylesheet\">\n\t<script src=\"main.js\" type=\"text/javascript\"></script>\n</head>\n<body>\n\t<div class=\"question\">This is the question</div>\n\t<div class=\"answer hidden\">This is the answer</div>\n\t<div class=\"toggle-button\" onclick=\"toggleAnswer()\">Toggle answer</div>\n</body>\n</html></code>\nstyle.css:\n<code lang=\"css\">.hidden {\n\tdisplay: none;\n}\n.toggle-button {\n\ttext-decoration: underline;\n\tcursor: pointer;\n}</code>\nmain.js:\n<code lang=\"javascript\">function toggleAnswer() {\n\tvar answer = document.getElementsByClassName(\"answer\")[0];\n\tif (answer.className == \"answer\") {\n\t\tanswer.className = \"answer hidden\";\n\telse {\n\t\tanswer.className = \"answer\";\n\t}\n}"
        "You found the concept of the answer appearing when a button is clicked really useful on the class website and you now want to replicate it. Write the HTML, CSS and JavaScript code to do so."
        "index.html:\n<code lang=\"html\"><html>\n<head>\n\t<link href=\"style.css\" rel=\"stylesheet\">\n\t<script src=\"main.js\" type=\"text/javascript\"></script>\n</head>\n<body>\n\t<div class=\"question\">This is the question</div>\n\t<div class=\"answer hidden\">This is the answer</div>\n\t<div class=\"toggle-button\" onclick=\"toggleAnswer()\">Toggle answer</div>\n</body>\n</html></code>\nstyle.css:\n<code lang=\"css\">.hidden {\n\tdisplay: none;\n}\n.toggle-button {\n\ttext-decoration: underline;\n\tcursor: pointer;\n}</code>\nmain.js:\n<code lang=\"javascript\">function toggleAnswer() {\n\tvar answer = document.getElementsByClassName(\"answer\")[0];\n\tif (answer.className == \"answer\") {\n\t\tanswer.className = \"answer hidden\";\n\telse {\n\t\tanswer.className = \"answer\";\n\t}\n}"
        False
      )
    , (Exercise
        "Votre ami a une drôle de façon de vous aider à étudier et plutôt que de vous dire les exercises à faire pour le cours de probabilités, il a créé un serveur avec un API pour les retourner. L'adresse de ceux-ci est\nhttp://www.exercisesdemath.com/semaine3. Écrivez une fonction JavaScript pour y accéder."
        "<code lang=\"javascript\">function getExercices(callback) {\n\tvar xhttp = new XMLHTTPRequest();\n\txhttp.onreadystatechanged = function() {\n\t\tif (this.readyState == 4 && this.status == 200) {\n\t\t\tvar response = JSON.parse(this.responseText);\n\t\t\tcallback(response);\n\t\t}\n\t};\n\txhttp.open(\"GET\", \"http://www.exercisesdemath.com/semaine3\");\n\txhttp.send();\n}</code>"
        "Your friend has a funny way of helping you study and instead of telling which exercices to do for you probability class, he built a server with an API to feed them. Their address is http://www.exercisesdemath.com/semaine3. Write a JavaScript function to get access to them."
        "<code lang=\"javascript\">function getExercices(callback) {\n\tvar xhttp = new XMLHTTPRequest();\n\txhttp.onreadystatechanged = function() {\n\t\tif (this.readyState == 4 && this.status == 200) {\n\t\t\tvar response = JSON.parse(this.responseText);\n\t\t\tcallback(response);\n\t\t}\n\t};\n\txhttp.open(\"GET\", \"http://www.exercisesdemath.com/semaine3\");\n\txhttp.send();\n}</code>"
        False
      )
    , (Exercise
        "Vous avez écrit la fonction suivante en Python et vous voudriez avoir la même chose en JavaScript. Réécrivez la pour avoir la même fonctionnalité. Vous devrez utiliser des boucles for à moins que vous connaissiez les fonctions de programmation fonctionnelle de JavaScript.\n<code lang=\"python\">def normalise(data: List[float]) -> List[foat]:\n\tmean = sum(data) / len(data)\n\tstd = (sum((x - mean)**2 for x in data) / len(data))**0.5\n\tnormalised = [(x - mean) / std for x in data]\n\treturn normalised</code>"
        "<code lang=\"javascript\">function normalise (data) {\n\tvar mean = 0;\n\tvar variance = 0;\n\tfor (var i = 0; i < data.length; i++) {\n\t\tmean = mean + data[i];\n\t}\n\tmean = mean / data.length;\n\tfor (var i = 0; i < data.length; i++) {\n\t\tvariance = variance + (data[i] - mean) ** 2;\n\t}\n\tvariance = variance / data.length;\n\tvar normalised = [];\n\tfor (var i = 0; i < data.length; i++) {\n\t\tnormalised.push((data[i] - mean) / variance ** 0.5);\n\t}\n\treturn normalised;\n}}</code>"
        "You wrote the following function in Python and you would want to have the same in JavaScript. Rewrite it to have the same functionaliy. You'll have to use for loops unless you know functional programming functions in JavaScript.\n<code lang=\"python\">def normalise(data: List[float]) -> List[foat]:\n\tmean = sum(data) / len(data)\n\tstd = (sum((x - mean)**2 for x in data) / len(data))**0.5\n\tnormalised = [(x - mean) / std for x in data]\n\treturn normalised</code>"
        "<code lang=\"javascript\">function normalise (data) {\n\tvar mean = 0;\n\tvar variance = 0;\n\tfor (var i = 0; i < data.length; i++) {\n\t\tmean = mean + data[i];\n\t}\n\tmean = mean / data.length;\n\tfor (var i = 0; i < data.length; i++) {\n\t\tvariance = variance + (data[i] - mean) ** 2;\n\t}\n\tvariance = variance / data.length;\n\tvar normalised = [];\n\tfor (var i = 0; i < data.length; i++) {\n\t\tnormalised.push((data[i] - mean) / variance ** 0.5);\n\t}\n\treturn normalised;\n}}</code>"
        False
      )
    ]


functionalProgrammingExercises : Exercises
functionalProgrammingExercises =
    [ (Exercise
        "Qu'est-ce qui caractérise une fonction pure?"
        "- N'a pas d'effets secondaires\n- Retourne toujours la même sortie avec les mêmes entrées\n- Ne modifie pas les entrées, mais retourne une copie modifiée"
        "What characterizes a pure fonction?"
        "- Doesn't have any side effects\n- Always returns the same output for the same inputs\n- Doesn't modify the inputs, but returns a modified copy"
        False
      )
    , (Exercise
        "Écrivez un générateur retournant la suite de Fibonacci."
        "<code lang=\"python\">function fibonacci() -> Generator[int, None, None]:\n\tx1, x2 = 1, None\n\tif x2 is None:\n\t\tyield x1\n\t\tx2 = 1\n\twhile True:\n\t\tyield x2\n\t\tx1, x2 = x2, x1 + x2</code>"
        "Write a generator to return numbers from the Fibonacci sequence."
        "<code lang=\"python\">function fibonacci() -> Generator[int, None, None]:\n\tx1, x2 = 1, None\n\tif x2 is None:\n\t\tyield x1\n\t\tx2 = 1\n\twhile True:\n\t\tyield x2\n\t\tx1, x2 = x2, x1 + x2</code>"
        False
      )
    , (Exercise
        "En utilisant uniquement les fonctions map, reduce et filter, donner la somme des hypothénuses des triplets suivants représentants les côtés de triangles rectangles (a^2 + b^2 = c^2).\n<code>data = [(3, 4, 5), (3, 4, 6), (2, 5, 10), (5, 12, 13), (7, 16, 17), (11, 60, 61)]"
        "<code>reduce(lambda s, x: s + x, map(lambda x: x[2], filter(lambda x: x[0]**2 + x[1]** 2 == x[2]**2, data)))</code>"
        "Using only the functions map, reduce and filter, give the sum of the hypotenuses of the following triplets reprenting the sides of rectangular triangles (a^2 + b^2 = c^2).\n<code>data = [(3, 4, 5), (3, 4, 6), (2, 5, 10), (5, 12, 13), (7, 16, 17), (11, 60, 61)]"
        "<code>reduce(lambda s, x: s + x, map(lambda x: x[2], filter(lambda x: x[0]**2 + x[1]** 2 == x[2]**2, data)))</code>"
        False
      )
    ]


dataVisualisationExercises : Exercises
dataVisualisationExercises =
    [ (Exercise
        "Avec le jeu de données suivant, écrivez le code pour produire un graphique permettant de juger de la corrélation entre le taux de meurtes et la consommation de crème glacée.\n<code>data = [{\"country\": ..., \"homicide_rate\": ..., \"ice_cream_consumption\": ...}, ...]</code>"
        "<code>plt.plot(\n\tlist(map(itemgetter(\"ice_cream_consumption\"), data)),\n\tlist(map(itemgetter(\"homicide_rate\"), data)),\n\t\"o\")\nplt.xlabel(\"Ice Cream Consumption\")\nplt.ylabel(\"Homicide Rate\")\nplt.title(\"Ice cream makes people murderous\")</code>"
        "With the following dataset, write the code to create a graphic to judge the correlation between the murder rate and ice cream consumption.\n<code>data = [{\"country\": ..., \"homicide_rate\": ..., \"ice_cream_consumption\": ...}, ...]</code>"
        "<code>plt.plot(\n\tlist(map(itemgetter(\"ice_cream_consumption\"), data)),\n\tlist(map(itemgetter(\"homicide_rate\"), data)),\n\t\"o\")\nplt.xlabel(\"Ice Cream Consumption\")\nplt.ylabel(\"Homicide Rate\")\nplt.title(\"Ice cream makes people murderous\")</code>"
        False
      )
    ]
