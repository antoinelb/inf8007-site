module Page.NotFound exposing (view)

import Html exposing (Html, br, div, text)
import Html.Attributes exposing (class, id)


view : Html msg
view =
    div [ id "notfound" ]
        [ text "404"
        , br [] []
        , text "The page couldn't be found"
        ]
