module Page.Home exposing (Model, view, viewEnglish)

import Data.Info exposing (Group)
import Date
import Html exposing (..)
import Html.Attributes exposing (..)


-- MODEL


type alias Model =
    { classGroups : List Group
    , labGroups : List Group
    , lastUpdate : Date.Date
    }



-- VIEW


view : Model -> Html msg
view model =
    div [ id "home-page" ]
        [ h1 [] [ text "Accueil" ]
        , h3 [] [ text <| "Attention: ce site peut être modifié en cours de trimestre (Dernière modification le " ++ (parseDate model.lastUpdate) ++ ")" ]
        , h2 [] [ text "Informations générales" ]
        , groupsTable model.classGroups
        , h2 [] [ text "Travaux pratiques" ]
        , groupsTable model.labGroups
        ]


viewEnglish : Model -> Html msg
viewEnglish model =
    div [ id "home-page" ]
        [ h1 [] [ text "Home" ]
        , h3 [] [ text <| "Warning: this website may be modified during the semester (last modification on " ++ (parseDate model.lastUpdate) ++ ")" ]
        , h2 [] [ text "General information" ]
        , groupsTableEnglish model.classGroups
        , h2 [] [ text "Labs" ]
        , groupsTableEnglish model.labGroups
        ]


parseDate : Date.Date -> String
parseDate date =
    (toString <| Date.month date) ++ "-" ++ (String.padLeft 2 '0' <| toString <| Date.day date) ++ " " ++ (String.padLeft 2 '0' <| toString <| Date.hour date) ++ ":" ++ (String.padLeft 2 '0' <| toString <| Date.minute date)


groupsTable : List Group -> Html msg
groupsTable groups =
    div [ class "group-table" ]
        (List.concat
            [ tableHeader
            , List.concatMap tableRow groups
            ]
        )


groupsTableEnglish : List Group -> Html msg
groupsTableEnglish groups =
    div [ class "group-table" ]
        (List.concat
            [ tableHeaderEnglish
            , List.concatMap tableRow groups
            ]
        )


tableHeader : List (Html msg)
tableHeader =
    [ div [ class "group-table-title" ] [ text "Groupe" ]
    , div [ class "group-table-title" ] [ text "Jour" ]
    , div [ class "group-table-title" ] [ text "Heure" ]
    , div [ class "group-table-title" ] [ text "Local" ]
    , div [ class "group-table-title" ] [ text "Enseignant(e)" ]
    ]


tableHeaderEnglish : List (Html msg)
tableHeaderEnglish =
    [ div [ class "group-table-title" ] [ text "Group" ]
    , div [ class "group-table-title" ] [ text "Day" ]
    , div [ class "group-table-title" ] [ text "Time" ]
    , div [ class "group-table-title" ] [ text "Room" ]
    , div [ class "group-table-title" ] [ text "Teacher" ]
    ]


tableRow : Group -> List (Html msg)
tableRow group =
    [ div [ class "group-table-row" ] [ text group.number ]
    , div [ class "group-table-row" ] [ text group.day ]
    , div [ class "group-table-row" ] [ text group.time ]
    , div [ class "group-table-row" ] [ text group.local ]
    , div [ class "group-table-row" ]
        [ text group.teacherName
        , br [] []
        , text group.teacherEmail
        , br [] []
        , text group.teacherOffice
        ]
    ]
