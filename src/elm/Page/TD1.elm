module Page.TD1 exposing (view, viewEnglish)

import Html exposing (..)
import Html.Attributes exposing (..)


view : Html msg
view =
    div [ class "td-page" ]
        [ h1 [] [ text "TD1 : analyse de texte" ]
        , h2 [] [ text "Description" ]
        , p [] [ text "Le premier TD consiste à écrire un programme en Python tout en pratiquant le traitement de chaînes de caractères." ]
        , p []
            [ text "Le travail consiste à calculer l'"
            , a [ href "http://en.wikipedia.org/wiki/Flesch-Kincaid_Readability_Test" ] [ text "indice de lisibilité Flesch (Flesch Reading Ease)" ]
            , text " de chacune des lignes du fichier "
            , a [ href "test.txt" ] [ text "test.txt" ]
            , text " et de donner le résultat dans un format similaire à "
            , a [ href "train_solution.txt" ] [ text "train_solution.txt" ]
            , text "."
            ]
        , p []
            [ text "Le fichier doit être utilisé de la façon suivante:"
            , br [] []
            , code [] [ text "python td1.py filename.txt" ]
            , br [] []
            , text "Et retourner un fichier sous le format "
            , code [] [ text "filename_solution.txt" ]
            ]
        , p []
            [ text "Pour que la séparation de syllables soit uniforme, il faut utiliser la librairie "
            , a [ href "http://pyphen.org/" ] [ text "pyphen" ]
            , text ". Pour la séparation des mots, bien que de bonnes librairies existent ("
            , a [ href "http://www.nltk.org/" ] [ text "nltk" ]
            , text "), il faut implémenter sa propre fonction de séparation et s'assurer qu'elles donnent les mêmes résultats que le fichier d'exemple."
            ]
        , p []
            [ text "Pour lire un fichier, le code suivant peut être utilisé comme exemple:"
            , br [] []
            , code [ lang "python" ]
                [ text "with open(\"filename.txt\", \"r\") as f:"
                , br [] []
                , span [ style [ ( "padding-left", "4ch" ) ] ] [ text "for line in f:" ]
                , br [] []
                , span [ style [ ( "padding-left", "8ch" ) ] ] [ i [] [ text "# do something" ] ]
                ]
            ]
        , h2 [] [ text "Documentation et liens utiles" ]
        , ul []
            [ li [] [ a [ href "https://docs.python.org/3/library/re.html" ] [ text "Expressions régulières" ] ]
            , li [] [ a [ href "http://pyphen.org/" ] [ text "Librairie pyphen" ] ]
            ]
        , h2 [] [ text "Fichiers" ]
        , ul []
            [ li [] [ a [ href "train.txt" ] [ text "train.txt" ], text " : fichier contenant les données sur lesquelles tester son programme" ]
            , li [] [ a [ href "train_solution.txt" ] [ text "train_solution.txt" ], text " : fichier contenant la solution des données d'entraînement" ]
            , li [] [ a [ href "test.txt" ] [ text "test.txt" ], text " : fichier contenant les données avec lesquelles le travail sera corrigé" ]
            ]
        , h2 [] [ text "Exigences et dépôt du travail" ]
        , ul []
            [ li [] [ text "Le travail doit être remis sur Moodle" ]
            , li []
                [ text "Le travail doit être remis dans un format \"zip\" sous le format:"
                , br [] []
                , code [] [ text "td1_matricule1.zip" ]
                ]
            , li []
                [ text "Le dossier doit contenir les fichiers suivants:"
                , br [] []
                , ol []
                    [ li [] [ code [] [ text "td1.py" ], text " : code principal pouvant être executé de la façon demandé dans la description" ]
                    , li [] [ code [] [ text "README.md" ], text " : contient le matricule et le nom de chacun des coéquipiers en plus de toute information jugée pertinente" ]
                    , li [] [ text "tout autre fichier nécessaire au fonctionnement du programme" ]
                    ]
                ]
            ]
        , h2 [] [ text "Barème" ]
        , ul []
            [ li [] [ text "Le code fonctionne avec le fichier ", code [] [ text "train.txt" ] ]
            , li [] [ text "Le code fonctionne avec le fichier ", code [] [ text "test.txt" ] ]
            , li [] [ text "Le code est bien commenté" ]
            , li [] [ text "Le code est bien organisé" ]
            , li [] [ text "Une bonne connaissance des expressions régulières est démontrée" ]
            , li [] [ text "Respecte les consignes de remise" ]
            , li [] [ b [] [ text "Bonus: " ], text "Le code est efficace" ]
            , li [] [ b [] [ text "Bonus: " ], text "Le code est réutilisable" ]
            ]
        , h2 [] [ text "Solution" ]
        , ul []
            [ li [] [ a [ href "tp1.py" ] [ text "tp1.py" ] ] ]
        ]


viewEnglish : Html msg
viewEnglish =
    div [ class "td-page" ]
        [ h1 [] [ text "TD1 : text analysis" ]
        , h2 [] [ text "Description" ]
        , p [] [ text "The first Td consists in writing a Python program while practicing character string treatment" ]
        , p []
            [ text "The works consits in computing the "
            , a [ href "http://en.wikipedia.org/wiki/Flesch-Kincaid_Readability_Test" ] [ text "Flesch Reading Ease metric" ]
            , text " for each line in the file "
            , a [ href "test.txt" ] [ text "test.txt" ]
            , text " and to return the result formatted like "
            , a [ href "train_solution.txt" ] [ text "train_solution.txt" ]
            , text "."
            ]
        , p []
            [ text "The file has to be used in the following way:"
            , br [] []
            , code [] [ text "python td1.py filename.txt" ]
            , br [] []
            , text "And return a file in the same format as "
            , code [] [ text "filename_solution.txt" ]
            ]
        , p []
            [ text "To make sure that the syllable transformation is the same for everyone, you must use the library "
            , a [ href "http://pyphen.org/" ] [ text "pyphen. " ]
            , text "For the work tokenisation, even though good libraries exist ("
            , a [ href "http://www.nltk.org/" ] [ text "nltk" ]
            , text "), you must implement you own tokenisation function and make sure it gives the same results as the example file."
            ]
        , p []
            [ text "To read a text file, the following code may be used as an example:"
            , br [] []
            , code [ lang "python" ]
                [ text "with open(\"filename.txt\", \"r\") as f:"
                , br [] []
                , span [ style [ ( "padding-left", "4ch" ) ] ] [ text "for line in f:" ]
                , br [] []
                , span [ style [ ( "padding-left", "8ch" ) ] ] [ i [] [ text "# do something" ] ]
                ]
            ]
        , h2 [] [ text "Documentation and useful links" ]
        , ul []
            [ li [] [ a [ href "https://docs.python.org/3/library/re.html" ] [ text "Regular expressions" ] ]
            , li [] [ a [ href "http://pyphen.org/" ] [ text "Library pyphen" ] ]
            ]
        , h2 [] [ text "Files" ]
        , ul []
            [ li [] [ a [ href "train.txt" ] [ text "train.txt" ], text " : file containing the data on which to test you program" ]
            , li [] [ a [ href "train_solution.txt" ] [ text "train_solution.txt" ], text " : file containg the solution to the training example" ]
            , li [] [ a [ href "test.txt" ] [ text "test.txt" ], text " : file containing the data on which the work will be corrected" ]
            ]
        , h2 [] [ text "Requirements and drop" ]
        , ul []
            [ li [] [ text "The work must be dropped on Moodle" ]
            , li []
                [ text "The work must be dropped in a \"zip\" format with the structure:"
                , br [] []
                , code [] [ text "td1_matricule1.zip" ]
                ]
            , li []
                [ text "The directory must contain the following files"
                , br [] []
                , ol []
                    [ li [] [ code [] [ text "td1.py" ], text " : main code that can be run in the way asked in the description" ]
                    , li [] [ code [] [ text "README.md" ], text " : contains the student id and name of each member of the team and any information judged useful" ]
                    , li [] [ text "any other file needed to run the program" ]
                    ]
                ]
            ]
        , h2 [] [ text "Evaluation" ]
        , ul []
            [ li [] [ text "The code works with the file ", code [] [ text "train.txt" ] ]
            , li [] [ text "The code works with the file ", code [] [ text "test.txt" ] ]
            , li [] [ text "The code is well commented" ]
            , li [] [ text "The code is well organised" ]
            , li [] [ text "A good understanding of regular expressions is shown" ]
            , li [] [ text "Respects the drop instructions" ]
            , li [] [ b [] [ text "Bonus: " ], text "The code is efficient" ]
            , li [] [ b [] [ text "Bonus: " ], text "The code is reusable" ]
            ]
        , h2 [] [ text "Solution" ]
        , ul []
            [ li [] [ a [ href "tp1.py" ] [ text "tp1.py" ] ] ]
        ]
