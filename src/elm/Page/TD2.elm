module Page.TD2 exposing (view, viewEnglish)

import Html exposing (..)
import Html.Attributes exposing (..)
import KaTeX exposing (render, renderToString, renderWithOptions, defaultOptions)


view : Html msg
view =
    div [ class "td-page" ]
        [ h1 [] [ text "TD2 : similarités de textes" ]
        , h2 [] [ text "Description" ]
        , p []
            [ text "L'objectif de ce TD est de déterminer la similarités entre des cours en se basant sur la similarité de leur description en utilisant le "
            , a [ href "https://en.wikipedia.org/wiki/Latent_semantic_analysis" ] [ text "LSA" ]
            , text ". Pour ce faire, il faudra encoder chacun des cours dans un espace vectoriel commun et utiliser la "
            , a [ href "https://en.wikipedia.org/wiki/Cosine_similarity" ] [ text "similarité du cosinus" ]
            , text " pour déterminer les distances."
            ]
        , p []
            [ text "On se rappelle que cette similarité entre deux vecteurs est calculée par:"
            , br [] []
            , cosineFormula
            ]
        , p []
            [ text "Pour transformer les descriptions en vecteurs, il vous faudra écrire votre propre fonction de séparation des mots. Si vous voulez raccourcir les mots à leur racine ("
            , i [] [ text "stemming" ]
            , text "), vous pouvez utiliser la librairie "
            , a [ href "http://www.nltk.org/" ] [ text "nltk" ]
            , text " et utiliser"
            ]
        , p [ class "center-text" ]
            [ code [] [ text "nltk.stem.snowball.FrenchStemmer" ]
            ]
        , p []
            [ text "Les vecteurs doivent être calculés en utilisant le "
            , a [ href "https://en.wikipedia.org/wiki/Tf%E2%80%93idf" ] [ i [] [ text "tf-idf" ] ]
            , text " de base, dont la formule pour un terme "
            , i [] [ text "t" ]
            , text " et un document "
            , i [] [ text "d" ]
            , text " est"
            , tfidfFormula
            , text "avec "
            , i [] [ text "tf" ]
            , text " représentant la nombre de fois où le terme "
            , i [] [ text "t" ]
            , text " apparaît dans le document "
            , i [] [ text "d" ]
            , text " et "
            , i [] [ text "idf" ]
            , text " calculé par"
            , idfFormula
            , text "c'est-à-dire le log de l'inverse de la proportion de documents où "
            , i [] [ text "t" ]
            , text " apparaît."
            ]
        , p []
            [ text "Chances sont que votre ordinateur n'aura pas la mémoire pour simplement utiliser numpy, alors une suggestion est d'utiliser les fonctions de "
            , a [ href "https://docs.scipy.org/doc/scipy/reference/sparse.html" ] [ text "scipy.sparse" ]
            , text ", dont l'utilisation sera expliquée en cours ou en laboratoire."
            ]
        , b []
            [ text "Pour ceux qui ne voudront pas prendre le temps d'écrire leur propre fonction, il est possible d'utiliser le "
            , a [ href "http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html" ] [ text "TfidfVectorizer" ]
            , text " de la librairie "
            , a [ href "http://scikit-learn.org/stable/" ] [ text "scikit-learn" ]
            , text ", mais cela entraînera une perte de 2 points."
            ]
        , p []
            [ text "Avant de calculer le cosinus, il faudra également réduire la dimension des vecteurs grâce au "
            , a [ href "https://en.wikipedia.org/wiki/Singular-value_decomposition" ] [ text "SVD" ]
            , text ", ce qui peut être fait grâce à la fonction "
            , a [ href "https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.svd.html" ] [ text "np.linalg.svd" ]
            , text " pour les matrices denses et "
            , a [ href "https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.svds.html" ] [ text "sp.sparse.linalg.svds" ]
            , text " pour les matrices creuses ("
            , i [] [ text "sparse" ]
            , text ")."
            ]
        , p []
            [ text "Une fois les similarités du cosinus calculées, le but est de retourner les 5 classes les plus similaires à celle donnée en argument et de les écrire dans un fichier. Un bon truc pour vérifier si votre code fonctionne est de vérifier que le cours le plus similaire à un cours donné est ce même cours."
            ]
        , p []
            [ text "Le fichier doit être utilisé de la façon suivant:"
            , br [] []
            , code [] [ text "python td2.py `class_code`" ]
            , br [] []
            , text "et écrire un fichier nommé "
            , code [] [ text "`class_code`.txt" ]
            , text " sous le même format que "
            , a [ href "td2_example.txt" ] [ text "td2_example.txt" ]
            , text "."
            ]
        , p []
            [ text "Le code de la classe doit pouvoir être écrit en majuscules ou en minuscules (oui c'est parce que je suis paresseux et que je n'aime pas écrire des majuscules). Des points seront accordés pour la présence de tests dans le travail remis (dans un fichier ou un dossier différent). Des points bonus seront également accordés pour ceux qui permettront de modifier certains paramètres à l'aide d'arguments sur la ligne de commande ("
            , a [ href "https://docs.python.org/3/library/argparse.html" ] [ text "argparse" ]
            , text ") pour la taille des vecteurs réduits ou le nombre de recommendations faites par exemple."
            ]
        , h2 [] [ text "Documentation et liens utiles" ]
        , ul []
            [ li [] [ a [ href "https://technowiki.wordpress.com/2011/08/27/latent-semantic-analysis-lsa-tutorial/" ] [ text "Tutoriel LSA" ] ]
            , li []
                [ a [ href "http://textminingonline.com/dive-into-nltk-part-iv-stemming-and-lemmatization" ] [ text "Exemple d'utilisation du stemming" ]
                ]
            ]
        , h2 [] [ text "Fichiers" ]
        , ul []
            [ li [] [ a [ href "cours.zip" ] [ text "cours.zip" ], text " : Données de cours" ]
            , li [] [ a [ href "td2_example.txt" ] [ text "td2_example.txt" ], text " : Exemple de remise de fichiers" ]
            ]
        , h2 [] [ text "Exigences et dépôt du travail" ]
        , ul []
            [ li [] [ text "Le travail doit être remis sur Moodle" ]
            , li []
                [ text "Le travail doit être remis dans un format \"zip\" sous le format:"
                , br [] []
                , code [] [ text "td2_matricule1_matricule2.zip" ]
                ]
            , li []
                [ text "Le dossier doit contenir les fichiers suivants:"
                , br [] []
                , ol []
                    [ li [] [ code [] [ text "td2.py" ], text " : code principal pouvant être executé de la façon demandé dans la description" ]
                    , li [] [ code [] [ text "README.md" ], text " : contient le matricule et le nom de chacun des coéquipiers en plus de toute information jugée pertinente" ]
                    , li [] [ code [] [ text "test_td2.py" ], text " ou ", code [] [ text "tests/" ], text " : fichier de tests ou dossier avec tous les tests" ]
                    , li [] [ text "tout autre fichier nécessaire au fonctionnement du programme" ]
                    ]
                ]
            ]
        , h2 [] [ text "Barème" ]
        , ul []
            [ li [] [ text "Le code fonctionne avec l'argument \"inf8007\"" ]
            , li [] [ text "Le code fonctionne avec l'argument \"stt2700\"" ]
            , li [] [ text "Le code fonctionne avec l'argument \"mat1720\"" ]
            , li [] [ text "Le code fonctionne avec un autre argument pris dans la liste des cours" ]
            , li [] [ text "Il y a des tests pour vérifier les fonctions principales" ]
            , li [] [ text "Les cours retournés ont un certain sens (pas nécessairement tous)" ]
            , li [] [ text "Les fonctions principales du code ne sont pas simplement importés d'autres librairies" ]
            , li [] [ text "Le code est bien commenté" ]
            , li [] [ text "Le code est bien organisé" ]
            , li [] [ text "Respecte les consignes de remise" ]
            , li [] [ b [] [ text "Bonus: " ], text "Le code est efficace" ]
            , li [] [ b [] [ text "Bonus: " ], text "Le code est réutilisable" ]
            , li [] [ b [] [ text "Bonus: " ], text "Il est possible de modifier des paramètres de la ligne de commande" ]
            ]
        , h2 [] [ text "Solution" ]
        , ul []
            [ li [] [ a [ href "tp2.py" ] [ text "tp2.py" ] ] 
            , li [] [ a [ href "test_tp2.py" ] [ text "test_tp2.py" ] ] ]
        ]


viewEnglish : Html msg
viewEnglish =
    div [ class "td-page" ]
        [ h1 [] [ text "TD2 : Text similarity" ]
        , h2 [] [ text "Description" ]
        , p []
            [ text "The objective of this TD is to determine the similarity between classes based on the similarity of their description using "
            , a [ href "https://en.wikipedia.org/wiki/Latent_semantic_analysis" ] [ text "LSA" ]
            , text ". To do this, we must encode each of the class in a common vector space and use the "
            , a [ href "https://en.wikipedia.org/wiki/Cosine_similarity" ] [ text "cosine similarity" ]
            , text " to determine the distances."
            ]
        , p []
            [ text "As seen, this similarity between two vectors is computed by:"
            , br [] []
            , cosineFormula
            ]
        , p []
            [ text "To transform the descriptions to vectors, you must write your own tokenisation function. If you want to stem the words, you may use the library "
            , a [ href "http://www.nltk.org/" ] [ text "nltk" ]
            , text " and use "
            ]
        , p [ class "center-text" ]
            [ code [] [ text "nltk.stem.snowball.FrenchStemmer" ]
            ]
        , p []
            [ text "The vectors have to computed using the basic "
            , a [ href "https://en.wikipedia.org/wiki/Tf%E2%80%93idf" ] [ i [] [ text "tf-idf" ] ]
            , text ", the formula for a term "
            , i [] [ text "t" ]
            , text " and a document "
            , i [] [ text "d" ]
            , text " is"
            , tfidfFormula
            , text "avec "
            , i [] [ text "tf" ]
            , text " which is the number of times where the term "
            , i [] [ text "t" ]
            , text " appears in the document "
            , i [] [ text "d" ]
            , text " and "
            , i [] [ text "idf" ]
            , text " computed by"
            , idfFormula
            , text "which is the log of the inverse of the proportion of document where "
            , i [] [ text "t" ]
            , text " appears."
            ]
        , p []
            [ text "Your computer fill probably not have enough memory to simply use numpy, so a susggestion is to use the functions of "
            , a [ href "https://docs.scipy.org/doc/scipy/reference/sparse.html" ] [ text "scipy.sparse" ]
            , text ", the use of which will be explained during class or the lab."
            ]
        , b []
            [ text "For those who will not want to take the time to write the time to write their own function, it's possible to use "
            , a [ href "http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html" ] [ text "TfidfVectorizer" ]
            , text " from the library "
            , a [ href "http://scikit-learn.org/stable/" ] [ text "scikit-learn" ]
            , text ", but this will cost a loss of 2 points."
            ]
        , p []
            [ text "Before computing the cosine, you must reduce the dimension of vectors with "
            , a [ href "https://en.wikipedia.org/wiki/Singular-value_decomposition" ] [ text "SVD" ]
            , text ", which may be done with the function "
            , text ", ce qui peut être fait grâce à la fonction "
            , a [ href "https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.svd.html" ] [ text "np.linalg.svd" ]
            , text " for dense matrices and "
            , a [ href "https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.svds.html" ] [ text "sp.sparse.linalg.svds" ]
            , text " for sparse matrices."
            ]
        , p []
            [ text "Once the cosine similarities will be computed, the goal is to return the 5 most similar classes to the one given as argument and to write them in a file. A good way to make sure that your code works is to verify that the most similar class to the one in argument is the class itself."
            ]
        , p []
            [ text "The file must be used in the following way:"
            , br [] []
            , code [] [ text "python td2.py `class_code`" ]
            , br [] []
            , text "and write and file named "
            , code [] [ text "`class_code`.txt" ]
            , text " in the same format as "
            , a [ href "td2_example.txt" ] [ text "td2_example.txt" ]
            , text "."
            ]
        , p []
            [ text "The class code has to be invariant to case (because I'm lazy and don't like writing uppercase). Points will be given for the presence of tests in the given work (in a different file or directory). Bonus points will also be awarded if certain parameters are modifiable thanks to command line arguments ("
            , a [ href "https://docs.python.org/3/library/argparse.html" ] [ text "argparse" ]
            , text ") for the reduced vector size or the number of recommendations made for example."
            ]
        , h2 [] [ text "Documentation and useful links" ]
        , ul []
            [ li [] [ a [ href "https://technowiki.wordpress.com/2011/08/27/latent-semantic-analysis-lsa-tutorial/" ] [ text "LSA tutorial" ] ]
            , li []
                [ a [ href "http://textminingonline.com/dive-into-nltk-part-iv-stemming-and-lemmatization" ] [ text "Stemming example" ]
                ]
            ]
        , h2 [] [ text "Files" ]
        , ul []
            [ li [] [ a [ href "cours.zip" ] [ text "cours.zip" ], text " : Class data" ]
            , li [] [ a [ href "td2_example.txt" ] [ text "td2_example.txt" ], text " : Example of submitted file" ]
            ]
        , h2 [] [ text "Reqirements and drop" ]
        , ul []
            [ li [] [ text "The work must be dropped on Moodle" ]
            , li []
                [ text "The work must be dropped in a \"zip\" format with the structure:"
                , br [] []
                , code [] [ text "td2_matricule1_matricule2.zip" ]
                ]
            , li []
                [ text "The directory must contain the following files"
                , br [] []
                , ol []
                    [ li [] [ code [] [ text "td2.py" ], text " : main code that can be run in the way asked in the description" ]
                    , li [] [ code [] [ text "README.md" ], text " : contains the student id and the name of each of the teammates as well as any information judged useful" ]
                    , li [] [ code [] [ text "test_td2.py" ], text " ou ", code [] [ text "tests/" ], text " : test file or directory containing the tests" ]
                    , li [] [ text "any other file necessary to run the program" ]
                    ]
                ]
            ]
        , h2 [] [ text "Evaluation" ]
        , ul []
            [ li [] [ text "The code works with the argument \"inf8007\"" ]
            , li [] [ text "The code works with the argument \"stt2700\"" ]
            , li [] [ text "The code works with the argument \"mat1720\"" ]
            , li [] [ text "The code works with another argument taken in the class list" ]
            , li [] [ text "There exists tests to verify the main functionalities of the code" ]
            , li [] [ text "The returned classes make a certain sense (not necessarely all of them)" ]
            , li [] [ text "The main functionalities aren't simply taken from other libraries" ]
            , li [] [ text "The code is well commented" ]
            , li [] [ text "The code is well organised" ]
            , li [] [ text "Respects the drop instructions" ]
            , li [] [ b [] [ text "Bonus: " ], text "The code is efficient" ]
            , li [] [ b [] [ text "Bonus: " ], text "the code is reusable" ]
            , li [] [ b [] [ text "Bonus: " ], text "It's possible to modify parameters from the command line" ]
            ]
        , h2 [] [ text "Solution" ]
        , ul []
            [ li [] [ a [ href "tp2.py" ] [ text "tp2.py" ] ] 
            , li [] [ a [ href "test_tp2.py" ] [ text "test_tp2.py" ] ] ]
        ]


cosineFormula : Html msg
cosineFormula =
    renderWithOptions { defaultOptions | displayMode = True } "\\frac{\\mathbf{x}\\mathbf{y}^T}{||\\mathbf{x}||\\,||\\mathbf{y}||}"


tfidfFormula : Html msg
tfidfFormula =
    renderWithOptions { defaultOptions | displayMode = True } "\\text{tf}(t, d) \\cdot \\text{idf}(t)"


idfFormula : Html msg
idfFormula =
    renderWithOptions { defaultOptions | displayMode = True } "\\text{idf}(t) = \\log\\frac{N}{|\\{d \\in D \\colon t \\in d \\}|}"
