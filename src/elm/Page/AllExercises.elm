module Page.AllExercises exposing (Model, Msg(..), update, view, viewEnglish)

import Data.Exercises exposing (AllExercises, Exercises, Exercise, ExerciseType(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Page.Exercises as ExercisesPage
import Route exposing (Route(..), ExerciseRoute(..), href)


type alias Model =
    AllExercises


type Msg
    = ComprehensionsMsg ExercisesPage.Msg
    | RegularExpressionsMsg ExercisesPage.Msg
    | TestsMsg ExercisesPage.Msg
    | NumpyMsg ExercisesPage.Msg
    | ServersMsg ExercisesPage.Msg
    | FrontendMsg ExercisesPage.Msg
    | FunctionalProgrammingMsg ExercisesPage.Msg
    | DataVisualisationMsg ExercisesPage.Msg


update : Msg -> AllExercises -> ( AllExercises, Cmd Msg )
update msg exercises =
    case msg of
        ComprehensionsMsg msg ->
            let
                ( comprehensions, cmd ) =
                    ExercisesPage.update msg exercises.comprehensions
            in
                ( { exercises | comprehensions = comprehensions }, Cmd.map ComprehensionsMsg cmd )

        RegularExpressionsMsg msg ->
            let
                ( regularExpressions, cmd ) =
                    ExercisesPage.update msg exercises.regularExpressions
            in
                ( { exercises | regularExpressions = regularExpressions }, Cmd.map RegularExpressionsMsg cmd )

        TestsMsg msg ->
            let
                ( tests, cmd ) =
                    ExercisesPage.update msg exercises.tests
            in
                ( { exercises | tests = tests }, Cmd.map TestsMsg cmd )

        NumpyMsg msg ->
            let
                ( numpy, cmd ) =
                    ExercisesPage.update msg exercises.numpy
            in
                ( { exercises | numpy = numpy }, Cmd.map NumpyMsg cmd )

        ServersMsg msg ->
            let
                ( servers, cmd ) =
                    ExercisesPage.update msg exercises.servers
            in
                ( { exercises | servers = servers }, Cmd.map ServersMsg cmd )

        FrontendMsg msg ->
            let
                ( frontend, cmd ) =
                    ExercisesPage.update msg exercises.frontend
            in
                ( { exercises | frontend = frontend }, Cmd.map FrontendMsg cmd )

        FunctionalProgrammingMsg msg ->
            let
                ( functionalProgramming, cmd ) =
                    ExercisesPage.update msg exercises.functionalProgramming
            in
                ( { exercises | functionalProgramming = functionalProgramming }, Cmd.map FunctionalProgrammingMsg cmd )

        DataVisualisationMsg msg ->
            let
                ( dataVisualisation, cmd ) =
                    ExercisesPage.update msg exercises.dataVisualisation
            in
                ( { exercises | dataVisualisation = dataVisualisation }, Cmd.map DataVisualisationMsg cmd )


view : Maybe ExerciseType -> Maybe Exercises -> Html Msg
view type_ exercises =
    case type_ of
        Nothing ->
            mainView

        Just ComprehensionsType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.view "Compréhensions" exercises |> Html.map ComprehensionsMsg

                Nothing ->
                    mainView

        Just RegularExpressionsType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.view "Expressions régulières" exercises |> Html.map RegularExpressionsMsg

                Nothing ->
                    mainView

        Just TestsType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.view "Tests" exercises |> Html.map TestsMsg

                Nothing ->
                    mainView

        Just NumpyType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.view "Numpy" exercises |> Html.map NumpyMsg

                Nothing ->
                    mainView

        Just ServersType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.view "Serveurs" exercises |> Html.map ServersMsg

                Nothing ->
                    mainView

        Just FrontendType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.view "Frontend" exercises |> Html.map FrontendMsg

                Nothing ->
                    mainView

        Just FunctionalProgrammingType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.view "Programmation fonctionnelle" exercises |> Html.map FunctionalProgrammingMsg

                Nothing ->
                    mainView

        Just DataVisualisationType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.view "Visualisation de données" exercises |> Html.map DataVisualisationMsg

                Nothing ->
                    mainView


mainView : Html msg
mainView =
    div [ id "all-exercises-page" ]
        [ h1 [] [ text "Exercices" ]
        , h2 [] [ text "Séance 2" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Comprehensions)) ] [ text "Compréhensions" ] ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just RegularExpressions)) ] [ text "Expressions régulières" ] ]
        , h2 [] [ text "Séance 3" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Tests)) ] [ text "Tests" ] ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Numpy)) ] [ text "Numpy" ] ]
        , h2 [] [ text "Séance 4" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Servers)) ] [ text "Serveurs" ] ]
        , h2 [] [ text "Séance 5" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Frontend)) ] [ text "Frontend" ] ]
        , h2 [] [ text "Séance 6" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just FunctionalProgramming)) ] [ text "Programmation fonctionnelle" ] ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just DataVisualisation)) ] [ text "Visualisation de données" ] ]
        ]


viewEnglish : Maybe ExerciseType -> Maybe Exercises -> Html Msg
viewEnglish type_ exercises =
    case type_ of
        Nothing ->
            mainViewEnglish

        Just ComprehensionsType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.viewEnglish "Comprehensions" exercises |> Html.map ComprehensionsMsg

                Nothing ->
                    mainViewEnglish

        Just RegularExpressionsType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.viewEnglish "Regular expressions" exercises |> Html.map RegularExpressionsMsg

                Nothing ->
                    mainViewEnglish

        Just TestsType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.viewEnglish "Tests" exercises |> Html.map TestsMsg

                Nothing ->
                    mainView

        Just NumpyType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.viewEnglish "Numpy" exercises |> Html.map NumpyMsg

                Nothing ->
                    mainView

        Just ServersType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.viewEnglish "Servers" exercises |> Html.map ServersMsg

                Nothing ->
                    mainView

        Just FrontendType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.viewEnglish "Frontend" exercises |> Html.map FrontendMsg

                Nothing ->
                    mainView

        Just FunctionalProgrammingType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.viewEnglish "Functional Programming" exercises |> Html.map FunctionalProgrammingMsg

                Nothing ->
                    mainView

        Just DataVisualisationType ->
            case exercises of
                Just exercises ->
                    ExercisesPage.viewEnglish "Data Visualisation" exercises |> Html.map DataVisualisationMsg

                Nothing ->
                    mainView


mainViewEnglish : Html msg
mainViewEnglish =
    div [ id "all-exercises-page" ]
        [ h1 [] [ text "Exercises" ]
        , h2 [] [ text "Class 2" ]
        , div [ class "exercises-item" ]
            [ a [ Route.href (Exercises (Just Comprehensions)) ] [ text "Comprehensions" ] ]
        , div [ class "exercises-item" ]
            [ a [ Route.href (Exercises (Just RegularExpressions)) ] [ text "Regular Expressions" ] ]
        , h2 [] [ text "Class 3" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Tests)) ] [ text "Tests" ] ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Numpy)) ] [ text "Numpy" ] ]
        , h2 [] [ text "Séance 4" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Servers)) ] [ text "Servers" ] ]
        , h2 [] [ text "Séance 5" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just Frontend)) ] [ text "Frontend" ] ]
        , h2 [] [ text "Séance 6" ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just FunctionalProgramming)) ] [ text "Functional Programming" ] ]
        , div [ class "exercises-item" ] [ a [ Route.href (Exercises (Just DataVisualisation)) ] [ text "Data Visualisation" ] ]
        ]
