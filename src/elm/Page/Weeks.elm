module Page.Weeks exposing (Model, view, viewEnglish)

import Data.Week exposing (Weeks, Week(..), Class, Slide, Chapter, Evaluation, Break)
import Date
import Html exposing (..)
import Html.Attributes exposing (..)
import Page.Header exposing (Language(..))


type alias Model =
    Weeks


view : Model -> Html msg
view model =
    div [ id "classes-page" ]
        [ h1 [] [ text "Détails des séances" ]
        , classesTable model
        ]


viewEnglish : Model -> Html msg
viewEnglish model =
    div [ id "classes-page" ]
        [ h1 [] [ text "Weekly class details" ]
        , classesTableEnglish model
        ]


parseDate : Maybe Date.Date -> String
parseDate date =
    case date of
        Just date ->
            (toString <| Date.month date) ++ "-" ++ (toString <| Date.day date) ++ " " ++ (toString <| Date.hour date) ++ ":" ++ (toString <| Date.minute date)

        Nothing ->
            ""


classesTable : Model -> Html msg
classesTable weeks =
    div [ class "class-table" ]
        (List.concat
            [ classesHeader
            , List.indexedMap classRow weeks |> List.concat
            ]
        )


classesTableEnglish : Model -> Html msg
classesTableEnglish weeks =
    div [ class "class-table" ]
        (List.concat
            [ classesHeaderEnglish
            , List.indexedMap classRowEnglish weeks |> List.concat
            ]
        )


classesHeader : List (Html msg)
classesHeader =
    [ div [ class "class-table-header" ] [ text "Semaine" ]
    , div [ class "class-table-header" ] [ text "Diapositives" ]
    , div [ class "class-table-header" ] [ text "Références (", a [ target "_blank", href "http://www.diveintopython3.net/" ] [ text "Pilgrim", text ")" ] ]
    , div [ class "class-table-header" ] [ text "Évaluation" ]
    , div [ class "class-table-header" ] [ text "Laboratoire" ]
    ]


classesHeaderEnglish : List (Html msg)
classesHeaderEnglish =
    [ div [ class "class-table-header" ] [ text "Week" ]
    , div [ class "class-table-header" ] [ text "Slides" ]
    , div [ class "class-table-header" ] [ text "References (", a [ target "_blank", href "http://www.diveintopython3.net/" ] [ text "Pilgrim", text ")" ] ]
    , div [ class "class-table-header" ] [ text "Evaluations" ]
    , div [ class "class-table-header" ] [ text "Labs" ]
    ]


classRow : Int -> Week -> List (Html msg)
classRow id class_ =
    case class_ of
        ClassWeek class_ ->
            [ div [ class "class-table-row" ] [ text (toString (id + 1)) ]
            , div [ class "class-table-row" ]
                [ text
                    ((Date.month
                        class_.french.week
                        |> toString
                     )
                        ++ "-"
                        ++ (Date.day class_.french.week + 1 |> toString)
                    )
                ]
            , slideList French class_.french.slides
            , chapterList class_.french.chapters
            , evaluationsList class_.french.evaluations
            , div [ class "class-table-row" ] (List.map text class_.french.labs)
            ]

        BreakWeek break ->
            [ div [ class "class-table-break-row" ] [ text (toString (id + 1)) ]
            , div [ class "class-table-break-row" ] [ text ((Date.month break.french.week |> toString) ++ "-" ++ (Date.day break.french.week + 1 |> toString)) ]
            , div [ class "class-table-break-row break-name-cell" ] [ text break.french.name ]
            ]


classRowEnglish : Int -> Week -> List (Html msg)
classRowEnglish id class_ =
    case class_ of
        ClassWeek class_ ->
            [ div [ class "class-table-row" ] [ text (toString (id + 1)) ]
            , div [ class "class-table-row" ]
                [ text
                    ((Date.month
                        class_.english.week
                        |> toString
                     )
                        ++ "-"
                        ++ (Date.day class_.english.week + 1 |> toString)
                    )
                ]
            , slideList English class_.english.slides
            , chapterList class_.english.chapters
            , evaluationsList class_.english.evaluations
            , div [ class "class-table-row" ] (List.map text class_.english.labs)
            ]

        BreakWeek break ->
            [ div [ class "class-table-break-row" ] [ text (toString (id + 1)) ]
            , div [ class "class-table-break-row" ] [ text ((Date.month break.english.week |> toString) ++ "-" ++ (Date.day break.english.week + 1 |> toString)) ]
            , div [ class "class-table-break-row break-name-cell" ] [ text break.english.name ]
            ]


slideList : Language -> List Slide -> Html msg
slideList language slides =
    div [ class "class-table-row" ]
        (List.map (slide language) slides)


slide : Language -> Slide -> Html msg
slide language s =
    let
        url =
            case language of
                English ->
                    if String.isEmpty s.englishUrl then
                        [ text s.text ]
                    else
                        [ a [ target "_blank", href s.englishUrl ] [ text s.text ] ]

                French ->
                    if String.isEmpty s.url then
                        [ text s.text ]
                    else
                        [ a [ target "_blank", href s.url ] [ text s.text ] ]

        otherUrl =
            case language of
                English ->
                    if String.isEmpty s.url then
                        [ text " (français)" ]
                    else
                        [ text " ("
                        , a [ target "_blank", href s.url ]
                            [ text
                                "français"
                            ]
                        , text ")"
                        ]

                French ->
                    if String.isEmpty s.englishUrl then
                        [ text " (english)" ]
                    else
                        [ text " ("
                        , a [ target "_blank", href s.englishUrl ]
                            [ text
                                "english"
                            ]
                        , text ")"
                        ]
    in
        p [] (List.concat [ url, otherUrl ])


chapterList : List Chapter -> Html msg
chapterList chapters =
    div [ class "class-table-row chapter-cell" ]
        (List.map chapter chapters)


chapter : Chapter -> Html msg
chapter c =
    if String.isEmpty c.url then
        text c.text
    else
        a [ target "_blank", href c.url ] [ text c.text ]


evaluationsList : List Evaluation -> Html msg
evaluationsList evaluations =
    div [ class "class-table-row" ]
        (List.map evaluation evaluations)


evaluation : Evaluation -> Html msg
evaluation e =
    if String.isEmpty e.url then
        p []
            [ text e.text
            , text e.otherText
            ]
    else
        p []
            [ a [ target "_blank", href e.url ] [ text e.text ]
            , text e.otherText
            ]
