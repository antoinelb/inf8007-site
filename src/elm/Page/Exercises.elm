module Page.Exercises exposing (Msg, update, view, viewEnglish)

import Data.Exercises exposing (Exercises, Exercise)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Views.Parser exposing (parseText)


type Msg
    = ToggleShownAnswer Int


update : Msg -> Exercises -> ( Exercises, Cmd Msg )
update msg exercises =
    case msg of
        ToggleShownAnswer index ->
            let
                toggleExercise i exercise =
                    if i == index then
                        { exercise | showingAnswer = not exercise.showingAnswer }
                    else
                        exercise
            in
                ( List.indexedMap toggleExercise exercises, Cmd.none )


view : String -> Exercises -> Html Msg
view title exercises =
    div [ id "exercises-page" ]
        [ h1 [] [ text title ]
        , div [ id "exercises" ] (List.indexedMap viewExercise exercises)
        ]


viewExercise : Int -> Exercise -> Html Msg
viewExercise index exercise =
    if exercise.showingAnswer then
        div [ class "exercise" ]
            [ div [ class "question" ]
                (List.concat
                    [ [ span [ class "bold" ] [ text "Question: " ] ]
                    , [ br [] [] ]
                    , parseText exercise.question
                    ]
                )
            , div [ class "answer" ]
                (List.concat
                    [ [ span [ class "bold" ] [ text "Réponse: " ] ]
                    , [ br [] [] ]
                    , parseText exercise.answer
                    ]
                )
            , div [ onClick (ToggleShownAnswer index), class "show-button" ] [ text "Cacher réponse" ]
            ]
    else
        div [ class "exercise" ]
            [ div [ class "question" ]
                (List.concat
                    [ [ span [ class "bold" ] [ text "Question: " ] ]
                    , [ br [] [] ]
                    , parseText exercise.question
                    ]
                )
            , div [ onClick (ToggleShownAnswer index), class "show-button" ] [ text "Afficher réponse" ]
            ]


viewEnglish : String -> Exercises -> Html Msg
viewEnglish title exercises =
    div [ id "exercises-page" ]
        [ h1 [] [ text title ]
        , div [ id "exercises" ] (List.indexedMap viewExerciseEnglish exercises)
        ]


viewExerciseEnglish : Int -> Exercise -> Html Msg
viewExerciseEnglish index exercise =
    if exercise.showingAnswer then
        div [ class "exercise" ]
            [ div [ class "question" ]
                (List.concat
                    [ [ span [ class "bold" ] [ text "Question: " ] ]
                    , [ br [] [] ]
                    , parseText exercise.questionEnglish
                    ]
                )
            , div [ class "answer" ]
                (List.concat
                    [ [ span [ class "bold" ] [ text "Answer: " ] ]
                    , [ br [] [] ]
                    , parseText exercise.answerEnglish
                    ]
                )
            , div [ onClick (ToggleShownAnswer index), class "show-button" ] [ text "Hide answer" ]
            ]
    else
        div [ class "exercise" ]
            [ div [ class "question" ]
                (List.concat
                    [ [ span [ class "bold" ] [ text "Question: " ] ]
                    , [ br [] [] ]
                    , parseText exercise.questionEnglish
                    ]
                )
            , div [ onClick (ToggleShownAnswer index), class "show-button" ] [ text "Show answer" ]
            ]
