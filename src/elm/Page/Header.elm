module Page.Header exposing (Language(..), Model, Msg, update, view)

import Data.Info exposing (Info)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)


type alias Model =
    { classInfo : Info
    , language : Language
    }


type Language
    = English
    | French


type Msg
    = ChangeLanguage


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeLanguage ->
            case model.language of
                English ->
                    ( { model | language = French }, Cmd.none )

                French ->
                    ( { model | language = English }, Cmd.none )


view : Model -> Html Msg
view model =
    let
        className =
            case model.language of
                English ->
                    model.classInfo.classNameEnglish

                French ->
                    model.classInfo.className

        classSession =
            case model.language of
                English ->
                    model.classInfo.classSessionEnglish

                French ->
                    model.classInfo.classSession

        shownLanguage =
            case model.language of
                English ->
                    "français"

                French ->
                    "english"
    in
        div [ id "header" ]
            [ img [ src "https://share.polymtl.ca/alfresco/guestDownload/direct?path=/Company%20Home/Sites/salle-de-presse---web/documentLibrary/logos/logoGenie/FR/gauche/polytechnique_genie_gauche_fr_rgb.png" ] []
            , div [ id "header-title" ]
                [ h1 [] [ text className ]
                , br [] []
                , h2 [] [ text classSession ]
                ]
            , div [ id "header-language", onClick ChangeLanguage ]
                [ text shownLanguage ]
            ]
