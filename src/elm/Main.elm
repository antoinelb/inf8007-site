module Main exposing (main)

import Data.Exercises exposing (ExerciseType(..))
import Html exposing (Html, program, div, text)
import Html.Attributes exposing (id)
import Config
import Navigation exposing (Location)
import Page.NotFound as NotFound
import Page.Home as Home
import Page.Weeks as Weeks
import Page.Plan as Plan
import Page.TD1 as TD1
import Page.TD2 as TD2
import Page.TD3 as TD3
import Page.Header as Header exposing (Language(..))
import Page.AllExercises as AllExercises
import Route exposing (Route, parseLocation)
import Views.Menu exposing (menu, menuEnglish)


type alias Model =
    { route : Route
    , home : Home.Model
    , weeks : Weeks.Model
    , header : Header.Model
    , exercises : AllExercises.Model
    }


initialModel : Model
initialModel =
    { route = Route.Home
    , home = Home.Model Config.classGroups Config.labGroups Config.lastUpdate
    , weeks = Config.weeks
    , header = Header.Model Config.classInfo French
    , exercises = Config.exercises
    }


init : Location -> ( Model, Cmd Msg )
init location =
    let
        route =
            parseLocation location
    in
        ( { initialModel | route = route }, Cmd.none )


type Msg
    = OnLocationChange Location
    | HeaderMsg Header.Msg
    | AllExercisesMsg AllExercises.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnLocationChange location ->
            let
                route =
                    parseLocation location
            in
                ( { model | route = route }, Cmd.none )

        HeaderMsg msg ->
            let
                ( header, cmd ) =
                    Header.update msg model.header
            in
                ( { model | header = header }, Cmd.map HeaderMsg cmd )

        AllExercisesMsg msg ->
            let
                ( exercises, cmd ) =
                    AllExercises.update msg model.exercises
            in
                ( { model | exercises = exercises }, Cmd.map AllExercisesMsg cmd )


view : Model -> Html Msg
view model =
    case model.route of
        Route.NotFound ->
            page model

        _ ->
            addPageElements (page model) model.header


page : Model -> Html Msg
page model =
    case model.header.language of
        English ->
            case model.route of
                Route.NotFound ->
                    NotFound.view

                Route.Home ->
                    Home.viewEnglish model.home

                Route.Weeks ->
                    Weeks.viewEnglish model.weeks

                Route.Plan ->
                    Plan.viewEnglish

                Route.TD1 ->
                    TD1.viewEnglish

                Route.TD2 ->
                    TD2.viewEnglish

                Route.TD3 ->
                    TD3.viewEnglish

                Route.Exercises Nothing ->
                    AllExercises.viewEnglish Nothing Nothing |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Comprehensions) ->
                    AllExercises.viewEnglish (Just ComprehensionsType) (Just model.exercises.comprehensions) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.RegularExpressions) ->
                    AllExercises.viewEnglish (Just RegularExpressionsType) (Just model.exercises.regularExpressions) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Tests) ->
                    AllExercises.viewEnglish (Just TestsType) (Just model.exercises.tests) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Numpy) ->
                    AllExercises.viewEnglish (Just NumpyType) (Just model.exercises.numpy) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Servers) ->
                    AllExercises.viewEnglish (Just ServersType) (Just model.exercises.servers) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Frontend) ->
                    AllExercises.viewEnglish (Just FrontendType) (Just model.exercises.frontend) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.FunctionalProgramming) ->
                    AllExercises.viewEnglish (Just FunctionalProgrammingType) (Just model.exercises.functionalProgramming) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.DataVisualisation) ->
                    AllExercises.viewEnglish (Just DataVisualisationType) (Just model.exercises.dataVisualisation) |> Html.map AllExercisesMsg

        French ->
            case model.route of
                Route.NotFound ->
                    NotFound.view

                Route.Home ->
                    Home.view model.home

                Route.Weeks ->
                    Weeks.view model.weeks

                Route.Plan ->
                    Plan.view

                Route.TD1 ->
                    TD1.view

                Route.TD2 ->
                    TD2.view

                Route.TD3 ->
                    TD3.view

                Route.Exercises Nothing ->
                    AllExercises.view Nothing Nothing |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Comprehensions) ->
                    AllExercises.view (Just ComprehensionsType) (Just model.exercises.comprehensions) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.RegularExpressions) ->
                    AllExercises.view (Just RegularExpressionsType) (Just model.exercises.regularExpressions) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Tests) ->
                    AllExercises.view (Just TestsType) (Just model.exercises.tests) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Numpy) ->
                    AllExercises.view (Just NumpyType) (Just model.exercises.numpy) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Servers) ->
                    AllExercises.view (Just ServersType) (Just model.exercises.servers) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.Frontend) ->
                    AllExercises.view (Just FrontendType) (Just model.exercises.frontend) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.FunctionalProgramming) ->
                    AllExercises.view (Just FunctionalProgrammingType) (Just model.exercises.functionalProgramming) |> Html.map AllExercisesMsg

                Route.Exercises (Just Route.DataVisualisation) ->
                    AllExercises.view (Just DataVisualisationType) (Just model.exercises.dataVisualisation) |> Html.map AllExercisesMsg


addPageElements : Html Msg -> Header.Model -> Html Msg
addPageElements page header =
    let
        menu_ =
            case header.language of
                English ->
                    menuEnglish

                French ->
                    menu
    in
        div [ id "page" ]
            [ Html.map HeaderMsg <| Header.view header
            , menu_
            , page
            ]


main : Program Never Model Msg
main =
    Navigation.program OnLocationChange
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        }
