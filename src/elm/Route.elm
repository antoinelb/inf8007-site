module Route exposing (Route(..), ExerciseRoute(..), href, modifyUrl, parseLocation)

import Html exposing (Attribute)
import Html.Attributes as Attr
import Navigation exposing (Location)


-- ROUTING


type Route
    = NotFound
    | Home
    | Weeks
    | Plan
    | TD1
    | TD2
    | TD3
    | Exercises (Maybe ExerciseRoute)


type ExerciseRoute
    = Comprehensions
    | RegularExpressions
    | Tests
    | Numpy
    | Servers
    | Frontend
    | FunctionalProgramming
    | DataVisualisation



-- INTERNAL


stringToRoute : String -> Route
stringToRoute route =
    case route of
        "" ->
            Home

        "#" ->
            Home

        "#notfound" ->
            NotFound

        "#weeks" ->
            Weeks

        "#plan" ->
            Plan

        "#td1" ->
            TD1

        "#td2" ->
            TD2

        "#td3" ->
            TD3

        "#exercises" ->
            Exercises Nothing

        "#exercises/comprehensions" ->
            Exercises (Just Comprehensions)

        "#exercises/regular-expressions" ->
            Exercises (Just RegularExpressions)

        "#exercises/tests" ->
            Exercises (Just Tests)

        "#exercises/numpy" ->
            Exercises (Just Numpy)

        "#exercises/servers" ->
            Exercises (Just Servers)

        "#exercises/frontend" ->
            Exercises (Just Frontend)

        "#exercises/functional-programming" ->
            Exercises (Just FunctionalProgramming)

        "#exercises/data-visualisation" ->
            Exercises (Just DataVisualisation)

        _ ->
            NotFound


routeToString : Route -> String
routeToString route =
    case route of
        NotFound ->
            "#notfound"

        Home ->
            "#"

        Weeks ->
            "#weeks"

        Plan ->
            "#plan"

        TD1 ->
            "#td1"

        TD2 ->
            "#td2"

        TD3 ->
            "#td3"

        Exercises Nothing ->
            "#exercises"

        Exercises (Just Comprehensions) ->
            "#exercises/comprehensions"

        Exercises (Just RegularExpressions) ->
            "#exercises/regular-expressions"

        Exercises (Just Tests) ->
            "#exercises/tests"

        Exercises (Just Numpy) ->
            "#exercises/numpy"

        Exercises (Just Servers) ->
            "#exercises/servers"

        Exercises (Just Frontend) ->
            "#exercises/frontend"

        Exercises (Just FunctionalProgramming) ->
            "#exercises/functional-programming"

        Exercises (Just DataVisualisation) ->
            "#exercises/data-visualisation"



-- HELPER FUNCTIONS


href : Route -> Attribute msg
href route =
    Attr.href (routeToString route)


parseLocation : Location -> Route
parseLocation location =
    stringToRoute location.hash


modifyUrl : Route -> Cmd msg
modifyUrl route =
    Navigation.modifyUrl (routeToString route)
