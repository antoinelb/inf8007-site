var path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    app: ['./src/js/index.js'],
  },

  output: {
    path: path.resolve(__dirname + '/dist'),
    filename: '[name].js',
  },

  module: {
    rules: [
      {
        test: /src\/css\/.+\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader',
        }),
      },
      {
        test: /src\/html\/.+.html$/,
        exclude: /node_modules/,
        loader: 'file-loader?name=[name].[ext]',
      },
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        loader: 'elm-webpack-loader?verbose=true&warn=true',
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff',
      },
      {
        test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        loaders: ['file-loader?name=[name].[ext]', 'image-webpack-loader'],
      },
      {
        test: /\.(pdf|txt|zip|py|md)$/,
        loader: 'file-loader?name=[name].[ext]',
      },
      {
        test: /src\/assets\/.+\.(js|html|css)$/,
        loader: 'file-loader?name=[name].[ext]',
      },
    ],

    noParse: /\.elm$/,
  },
  plugins: [
    new ExtractTextPlugin('styles.css'),
  ],

  devServer: {
    inline: true,
    stats: {colors: true},
  },
};
