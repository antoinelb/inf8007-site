#!/bin/bash
python copy_files.py
sed -i -e "16 s/[0-9]*-[0-9]*-[0-9]* [0-9]*:[0-9]*/$(date '+%Y-%m-%d %H:%M')/" src/elm/Config.elm
yarn build
scp dist/* inf8007@www.groupes.polymtl.ca:/data/cours/inf8007
