#!/usr/bin/python3
import os
import shutil

assets_path = os.path.join(os.path.dirname(__file__), "src", "assets")

# copy all slides
slides_path = os.path.join(os.path.dirname(__file__), os.pardir, "class")

for class_ in os.listdir(slides_path):
    if class_.startswith("class"):
        for slide in os.listdir(os.path.join(slides_path, class_)):
            for file_ in os.listdir(os.path.join(slides_path, class_, slide)):
                if file_.endswith("handout.pdf"):
                    shutil.copy(
                        os.path.join(slides_path, class_, slide, file_),
                        os.path.join(
                            assets_path,
                            "slides",
                            file_.replace("_handout", ""),
                        ),
                    )

# copy tp files
tp_path = os.path.join(os.path.dirname(__file__), os.pardir, "tp")

for tp in os.listdir(tp_path):
    if not os.path.exists(os.path.join(assets_path, tp)):
        os.mkdir(os.path.join(assets_path, tp))
    if "files" in os.listdir(os.path.join(tp_path, tp)):
        for file_ in os.listdir(os.path.join(tp_path, tp, "files")):
            shutil.copy(
                os.path.join(tp_path, tp, "files", file_),
                os.path.join(assets_path, tp, file_),
            )
